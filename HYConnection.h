//
//  HYConnection.h
//  HYLevelMaker
//
//  Created by Albert  Leung on 4/15/09.
//  Copyright 2009 WhiffleBird. All rights reserved.
//

#import <Foundation/Foundation.h>
@class LevelTile;

@interface HYConnection : NSObject 

@property (strong, nonatomic) LevelTile *source;
@property (strong, nonatomic) LevelTile *target;
@property (strong, nonatomic) NSBezierPath *connectionLine;

@end
