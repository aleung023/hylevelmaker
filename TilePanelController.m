//
//  TilePanelController.m
//  HYLevelMaker
//
//  Created by Albert on 4/6/09.
//  Copyright 2009 Whifflebird. All rights reserved.
//

#import "TilePanelController.h"


@implementation TilePanelController

- (id)init
{
	if (![super initWithWindowNibName:@"TilePanel"])
		return nil;
	return self;
}

@end
