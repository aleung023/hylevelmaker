//
//  MyDocument.m
//  HYLevelMaker
//
//  Created by Albert on 4/5/09.
//  Copyright Whifflebird 2009 . All rights reserved.
//

#import "MyDocument.h"

@interface MyDocument ()

@property (weak, nonatomic) IBOutlet NSTextField *name;
@property (weak, nonatomic) IBOutlet NSTextField *clue;
@property (weak, nonatomic) IBOutlet LevelView *levelView;
@property (weak, nonatomic) IBOutlet NSTextField *bonusSliderLabel;
@property (weak, nonatomic) IBOutlet NSSlider *bonusSlider;
@property (weak, nonatomic) IBOutlet NSTextField *firstLevel;
@property (weak, nonatomic) IBOutlet NSTextField *nextLevel;
@property (weak, nonatomic) IBOutlet NSTextField *roomA;
@property (weak, nonatomic) IBOutlet NSTextField *roomB;
@property (weak, nonatomic) IBOutlet NSTextField *roomC;
@property (weak, nonatomic) IBOutlet NSTextField *roomD;
@property (weak, nonatomic) IBOutlet NSImageView *snapShot;
@property (weak, nonatomic) IBOutlet NSButton *startLevelCheckBox;
@property (weak, nonatomic) IBOutlet NSButton *isFreeLevelCheckBox;
@property (strong, nonatomic) HYLevel *level;
@property (strong, nonatomic) CIFilter *scaleTransformFilter;

- (IBAction)bonusSliderChanged:(id)sender;
- (IBAction)takeSnapShot:(id)sender;

@end

@implementation MyDocument

- (id)init
{
    self = [super init];
    if (self) {
		self.scaleTransformFilter = [CIFilter filterWithName:@"CILanczosScaleTransform"];
        [self.scaleTransformFilter setDefaults];
		// We need to scale the image to width = 124, height = 179 from width = 288, height = 416
		float scaleH = 0.43;
		float aspect = 1.0f;
		[self.scaleTransformFilter setValue:[NSNumber numberWithFloat:scaleH] forKey:@"inputScale"];
		[self.scaleTransformFilter setValue:[NSNumber numberWithFloat:aspect] forKey:@"inputAspectRatio"];
    }
    return self;
}

- (IBAction)bonusSliderChanged:(id)sender {
	NSString *bonusSliderLabelText = [[NSString alloc] initWithFormat:@"Bonus %d", [self.bonusSlider intValue]];
	[self.bonusSliderLabel setStringValue:bonusSliderLabelText];
}

#pragma mark -
#pragma mark === Data and Window ===
#pragma mark -
- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"MyDocument";
}

/**
 * After document is read from disk or initialized update the view.
 */
- (void)windowControllerDidLoadNib:(NSWindowController *) aController
{
	[self.bonusSliderLabel setStringValue:@"Bonus 100"];
	// Initialize booleans.
	self.startLevelCheckBox.state = NSOffState;
	self.isFreeLevelCheckBox.state = NSOffState;
	if (self.level != nil) {
		[self.name setStringValue:self.level.name];
		[self.clue setStringValue:self.level.clue];
		[self.bonusSlider setIntValue:self.level.bonus];
		NSString *bonusSliderLabelText = [[NSString alloc] initWithFormat:@"Bonus %d", self.level.bonus];
		[self.bonusSliderLabel setStringValue:bonusSliderLabelText];
		if (self.level.map != nil && [self.level.map count] > 0) {
			NSMutableArray *tiles = self.levelView.tiles;
			NSMutableArray *values = self.level.map;
			int i;
			for (i = 0; i < kNumberOfLevelTiles; i++) {
				LevelTile *tile = [tiles objectAtIndex:i];
				NSNumber *value = [values objectAtIndex:i];
				tile.value = [value intValue];
			}	
			[self.levelView updateImagesToTiles];
		}
		[self.levelView recreateConnections:self.level.connections];
		[self.firstLevel setStringValue:self.level.firstLevel == nil ? @"" : self.level.firstLevel];
		[self.nextLevel setStringValue:self.level.nextLevel];
		[self.roomA setStringValue:self.level.roomA];
		[self.roomB setStringValue:self.level.roomB];
		[self.roomC setStringValue:self.level.roomC];
		[self.roomD setStringValue:self.level.roomD];
		// Convert the data to an NSImage.
		NSImage *image = [[NSImage alloc] initWithData:self.level.snapShot];
		[self.snapShot setImage:image];
		self.startLevelCheckBox.state = self.level.startLevelOfMap ? NSOnState : NSOffState;
		self.isFreeLevelCheckBox.state = self.level.isFreeMap ? NSOnState : NSOffState;
	} 
	[super windowControllerDidLoadNib:aController];
}

- (IBAction)takeSnapShot:(id)sender {
	// Get the snap shot from the LevelView!
	self.levelView.doSnapShot = YES;
	[self.levelView setNeedsDisplay:YES];
	// Get the PNG data representation of this view to be
	// compatible with the iPhone.
	//
	// Get a bitmap object to cache the view into. This object goes into the 
	// autorelease pool.
	NSBitmapImageRep *bitmap = [self.levelView bitmapImageRepForCachingDisplayInRect:[self.levelView bounds]];
	// Now put the view into the bitmap.
	[self.levelView cacheDisplayInRect:[self.levelView bounds] toBitmapImageRep:bitmap];
	// We need to scale the image to width = 124, height = 179 from width = 288, height = 416
	CIImage *inputCIImage = [[CIImage alloc] initWithBitmapImageRep:bitmap];
	[self.scaleTransformFilter setValue:inputCIImage forKey:@"inputImage"];
	CIImage *outputCIImage = [self.scaleTransformFilter valueForKey:@"outputImage"];
	NSImage *result = [[NSImage alloc] initWithSize:NSMakeSize(124, 179)];	
	NSCIImageRep *ciImageRep = [NSCIImageRep imageRepWithCIImage:outputCIImage];
	[result addRepresentation:ciImageRep];
	[self.snapShot setImage:result];
	[self.snapShot setNeedsDisplay:YES];
	self.levelView.doSnapShot = NO;
	[self.levelView setNeedsDisplay:YES];
}

/**
 * Save the level here.
 */
- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
	self.level = [[HYLevel alloc] init];
	self.level.name = [self.name stringValue];
	self.level.clue = [self.clue stringValue];
	self.level.bonus = [self.bonusSlider intValue];
	// Extract the integer values from the view.
	NSMutableArray *mapValues = [[NSMutableArray alloc] init];
	for (LevelTile *tile in [self.levelView tiles]) {
		[mapValues addObject:[NSNumber numberWithInt:tile.value]];
	}
	self.level.map = mapValues;
	self.level.firstLevel = [self.firstLevel stringValue];
	self.level.nextLevel = [self.nextLevel stringValue];
	self.level.roomA = [self.roomA stringValue];
	self.level.roomB = [self.roomB stringValue];
	self.level.roomC = [self.roomC stringValue];
	self.level.roomD = [self.roomD stringValue];
	self.level.startLevelOfMap = self.startLevelCheckBox.state == NSOnState;
	self.level.isFreeMap = self.isFreeLevelCheckBox.state == NSOnState;
	// Get the snap shot from the LevelView!
	[self takeSnapShot:nil];
	// Get the PNG data representation of this view to be
	// compatible with the iPhone.
	//
	// Get a bitmap object to cache the view into. This object goes into the 
	// autorelease pool.
	NSBitmapImageRep *bitmap = [self.snapShot bitmapImageRepForCachingDisplayInRect:[self.snapShot bounds]];
	// Now put the view into the bitmap.
	[self.snapShot cacheDisplayInRect:[self.snapShot bounds] toBitmapImageRep:bitmap];
	// Get a PNG representation of this bitmap. The properties refer to image options like
	// compression, color table, interlace, etc. The PNG representation gets rid of
	// the grid lines for some reason. PNG image file format is the only one compatible with
	// the iPhone.
	NSData *data = [bitmap representationUsingType:NSPNGFileType properties:nil];
	self.level.snapShot = data;
	// Save the connections too.
	if ([self.levelView.connections count] > 0) {
		NSMutableArray *tilePairs = [[NSMutableArray alloc] init];
		for (HYConnection *connection in self.levelView.connections) {
			[tilePairs addObject:[NSNumber numberWithInt:connection.source.index]];
			[tilePairs addObject:[NSNumber numberWithInt:connection.target.index]];
		}
		self.level.connections = tilePairs;
	}
	return [NSKeyedArchiver archivedDataWithRootObject:self.level];
}

/**
 * Read the level here.
 */
- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{   
	@try {
		self.level = [NSKeyedUnarchiver unarchiveObjectWithData:data];
	}
	@catch (NSException *e) {
		if (outError) {
			NSDictionary *d = [NSDictionary dictionaryWithObject:@"The data is corrupted." forKey:NSLocalizedFailureReasonErrorKey];
			*outError = [NSError errorWithDomain:NSOSStatusErrorDomain code:unimpErr userInfo:d];
		}
		return NO;
	}
	return YES;			
}

@end
