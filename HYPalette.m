//
//  Palette.m
//  HydrogenLevelMaker
//
//  Created by Albert  Leung on 4/2/09.
//  Copyright 2009 WhiffleBird. All rights reserved.
//

#import "HYPalette.h"


@implementation HYPalette

- (BOOL)isPointInPallete:(NSPoint)p {
	// +---+ <-- y1
	// |   |  
	// +---+ <-- y2
	// 
	// ^   ^
	// x1  x2
	//
	float y1 = self.rect.origin.y + self.rect.size.height;
	float y2 = self.rect.origin.y;
	float x1 = self.rect.origin.x;
	float x2 = self.rect.origin.x + self.rect.size.width;
	if ((p.x < x2 && p.x > x1) && (p.y < y1 && p.y > y2)) {
		return YES;
	} else {
		return NO;
	}
}

- (NSPoint)getCenter {
	float x = self.rect.origin.x + self.rect.size.width / 2.0;
	float y = self.rect.origin.y + self.rect.size.height / 2.0;
	return NSMakePoint(x, y);
}

@end
