//
//  AppController.h
//  HYLevelMaker
//
//  Created by Albert  Leung on 4/3/09.
//  Copyright 2009 WhiffleBird. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class TilePanelController;

@interface AppController : NSObject

@property (strong, nonatomic) TilePanelController *tilePanelController;

- (IBAction)showTilePanel:(id)sender;

@end
