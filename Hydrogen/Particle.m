//
//  Particle.m
//  Hydrogen
//
//  Created by Albert on 1/13/09.
//  Copyright 2009 whifflebird.com. All rights reserved.
//

#import "Particle.h"

/*
 * Anything faster than this will pass through walls.
 */
#define kSpeedOfLight				600.0 
#define kGravity					3800.0
#define kVelocityLoss				0.80
#define kMinimumOpacity				0.2f
#define kMinimumScale				0.1f

/*
 * The CoR or coefficient of restitution is defined as
 * -(v2f - v1f) / (v2 - v1) where 1 and 2 are for particles 
 * 1 and 2 and f denotes final velocity
 */
#define kCoR						0.90

/**
 * Coulomb's constant in this world
 */
#define kE							800000.0
#define kMinVelocity				1.0
#define kEpsilon					0.1
// Define constants that define the behavior
// of EM, and spring force for particle interactions 
/*
 * The ratio of kMaxStrongInteraction/kMaxRestore determines how much
 * crushing there is between the two particles when they are stuck 
 * together. It also determines how much vibration there is. The lower
 * the ratio the greater the vibration. The higher the ratio, approaching
 * 1.0, the greater the crushing.
 */
#define kMaxStrongInteraction		1600.0 
#define kMaxRestore					2000.0
#define kSpring						1700.0
#define kRestoreDistanceFactor		1.2
/*
 * Descreasing this lowers vibration but it slows down their velocity when they 
 * are stuck together.
 */
#define kAbsorption					0.89

@interface Particle (PrivateMethods)

- (void)record;
- (void)rewind;
- (void)resetFilm;
- (BOOL)outOfFilm;
- (void)separateParticles:(Particle *)p;

@end

@implementation Particle {
    float velocity[2]; // velocity in 2 dimensions
    int filmIndex;
    int filmLength;
    // Recorded positions, velocity and acceleration.
    float film[kFilmLength][10]; // Keep a record of the 10 last x, y, Vx, Vy, Ax, Ay, Px, Py, Kx, Ky
}

#pragma mark -
#pragma mark === Setting up and tearing down ===
#pragma mark -

- (instancetype)init {
    self = [super init];
	if (self) {
		filmIndex = 0;
		filmLength = 0;
		velocity[0] = 0.0f;
		velocity[1] = 0.0f;
		self.accGx = 0.0f;
		self.accGy = 0.0f;
		self.accPx = 0.0f;
		self.accPy = 0.0f;
		self.gravity = HYWeakPlus;
		self.charge = HYStrongZero;
		self.state = HYStable;
		self.sequence = 0;
		self.discharged = NO;
		self.massless = NO;
		self.charm = 0;
		self.shields = 0;
		self.image = nil;
		self.shatteredImage = nil;
		self.imageSequence = nil;
		self.drownSequence = nil;
		self.fallingSequence = nil;
		self.breakSequence = nil;
		self.charmSequence = nil;
		self.opacity = 1.0f;
		self.rotation = 0.0f;
		self.scale = CGPointMake(1.0f, 1.0f);
		self.invisible = NO;
	}
	return self;
}

#pragma mark - 
#pragma mark === Film ===
#pragma mark -

/*
 * Put the most recent at position 0 and move the rest down.
 * For each row the columns represent:
 * x, y		position (0, 1)
 * Vx, Vy	velocity in x and y directions (2, 3)
 * Ax, Ay	acceleration due to gravity in x and y directions (4, 5)
 * Px, Py	acceleration due to particle interactions in x and y directions (6, 7)
 * Kx, Ky	acceleration due to particle spring interactions in x and y directions (8, 9)
 *
 */
- (void)record {
	// Move everybody down first.
	filmLength++;
	filmLength = filmLength > kFilmLength ? kFilmLength : filmLength;
	for (int i = 0; i < kFilmLength; i++)
	{
		if (i + 1 == kFilmLength) {
			break;
		}
		for (int j = 0; j < 8; j++) {
			film[i + 1][j] = film[i][j];
		}
	}
	// Put the most recent values here.
	film[0][0] = self.x;
	film[0][1] = self.y;
	film[0][2] = velocity[0];
	film[0][3] = velocity[1];
	film[0][4] = self.accGx;
	film[0][5] = self.accGy;
	film[0][6] = self.accPx;
	film[0][7] = self.accPy;
	film[0][8] = self.accKx;
	film[0][9] = self.accKy;
}

- (void)rewind {
	if (filmIndex == filmLength) {
		return;
	}
	self.x = film[filmIndex][0];
	self.y = film[filmIndex][1];
	velocity[0] = film[filmIndex][2];
	velocity[1] = film[filmIndex][3];
	self.accGx = film[filmIndex][4];
	self.accGy = film[filmIndex][5];
	self.accPx = film[filmIndex][6];
	self.accPy = film[filmIndex][7];
	self.accKx = film[filmIndex][8];
	self.accKy = film[filmIndex][9];
	filmIndex++;
}

- (void)resetFilm {
	filmIndex = 0;
}

- (BOOL)outOfFilm {
	if (filmIndex == kFilmLength) {
		return YES;
	} else {
		return NO;
	}
}

#pragma mark -
#pragma mark === Velocity, speed and position ===
#pragma mark -
/*
 * Absorb energy. Velocity will decrease by 
 * percentage p.
 */
- (void)absorb:(float)p {
	velocity[0] *= p;
	velocity[1] *= p;
}

/**
 * Update the particle's new position.
 * AccX, AccY is the acceleration due to gravity coming from the accelerometer.
 * Qx, Qy is the acceleration due to EM from different particles and tiles and is
 * accumulated. Kx and Ky is the acceleration due to shape interaction and can also
 * be accumulated.
 */
- (void)updatePositionWithAccX:(float)accX AccY:(float)accY Qx:(float)qX Qy:(float)qY Kx:(float)kX Ky:(float)kY {
#if 0
	// Record the old values first.
	[self record];
#endif
	switch (self.gravity) {
		case HYStateWeakZero:
		case HYWeakZero:
			// There is no interaction with gravity
			accX = 0.0f;
			accY = 0.0f;
			break;
		case HYWeakMinus:
			// There is an anti-reaction with gravity
			accX = -accX;
			accY = -accY;
			break;
		default:
			break;
	}
	self.accGx = accX * kGravity;
	self.accGy = accY * kGravity;
	self.accPx += qX;
	self.accPy += qY;
	self.accKx += kX;
	self.accKy += kY;
	// Apply special relativity.
	float gamma = 0.0f;
	float beta = (velocity[0] * velocity[0] + velocity[1] * velocity[1]) / (kSpeedOfLight * kSpeedOfLight);
	if (beta < 1.0) {
		gamma = sqrt(1.0 - beta);
	}
	
	/*
	if (type == HYElectron) {
		NSLog(@"accGx = %f, accGy = %f, accPx = %f, accPy = %f, accKx = %f, accKy = %f", accGx, accGy, accPx, accPy, accKx, accKy);		
	}
	*/
	float ax = (self.accGx + self.accPx + self.accKx) * gamma;
	float ay = (self.accGy + self.accPy + self.accKy) * gamma;

	// Calculate new velocity and position.
	velocity[0] = velocity[0] + ax * self.timeInterval;
	velocity[1] = velocity[1] + ay * self.timeInterval;
	// Apply friction
	velocity[0] *= self.f;
	velocity[1] *= self.f;
	self.x = velocity[0] * self.timeInterval + self.x;
	self.y = velocity[1] * self.timeInterval + self.y;
}

- (void)reflectVx {
	velocity[0] = -velocity[0] * kVelocityLoss;
}

- (void)reflectVy {
	velocity[1] = -velocity[1] * kVelocityLoss;
}

- (void)reflectVxNoLoss {
	velocity[0] = -velocity[0];
}

- (void)reflectVyNoLoss {
	velocity[1] = -velocity[1];
}

- (float)getVx {
	return velocity[0];
}

- (void)setVx:(float)v {
	velocity[0] = v;
}

- (float)getVy {
	return velocity[1];
}

- (void)setVy:(float)v {
	velocity[1] = v;
}

- (float)speed {
	return hypot(velocity[0], velocity[1]);
}

- (float)Ex {
	return 0.5 * self.m * velocity[0]*velocity[0];
}

- (float)Ey {
	return 0.5 * self.m * velocity[1]*velocity[1];
}

- (float)energy {
	return 0.5*self.m*(velocity[0]*velocity[0] + velocity[1]*velocity[1]);
}

#pragma mark -
#pragma mark === Particle interactions ===
#pragma mark -
- (float)distanceFromParticle:(Particle *)p {
	// Compute the distance between the 2 particles
	return hypot(self.x - p.x, self.y - p.y);
}

- (BOOL)determineCollisionWithParticle:(Particle *)p {
	// Compute the distance between the 2 particles
	float d = hypot(self.x - p.x, self.y - p.y);
	if (d <= (self.r + p.r)) {
		// A collision has occurred.
		return YES;
	} else {
		return NO;
	}
}

/*
 * Compute the new velocities after the collision.
 */
- (void)handleCollisionWithParticle:(Particle *)p {
	float d = hypot(p.x - self.x, p.y - self.y);
	if (velocity[0] < kMinVelocity && velocity[1] < kMinVelocity && [p getVx] < kMinVelocity && [p getVy] < kMinVelocity) {
		return;
	}
	if (d < kEpsilon) {
		return;
	}
	// Find the angle of the collision
	double phi = atan((p.y - self.y)/(p.x - self.x));
	// NSLog(@"Velocities at collision V1x = %f, V1y = %f, V2x = %f, V2y = %f", velocity[0], velocity[1], [p getVx], [p getVy]);
	// Find the velocity component that is colinear with the centers 
	// of the two particles.
	float cos_phi = fabs(cos(phi));
	float sin_phi = fabs(sin(phi));
	cos_phi = p.x > self.x ? cos_phi : -cos_phi;
	sin_phi = p.y > self.y ? sin_phi : -sin_phi;
	float Vn = velocity[0]*cos_phi + velocity[1]*sin_phi;
	// The velocity component that is tangent to the two particles.
	float Vt = velocity[1]*cos_phi - velocity[0]*sin_phi;
	// Do the same with particle p.
	float Vnp = [p getVx]*cos_phi + [p getVy]*sin_phi;
	float Vtp = [p getVy]*cos_phi - [p getVx]*sin_phi;
	// NSLog(@"Vn = %f, Vt = %f, Vnp = %f, Vtp = %f", Vn, Vt, Vnp, Vtp);
	// The velocity that is tangent remains unchanged. The velocity
	// That is normal to the two particles changes as a function of the
	// coefficient of restitution and the conservation of momentum.
	float _Vn = ((kCoR + 1.0)*p.m*Vnp + Vn*(self.m - kCoR*p.m))/(self.m + p.m);
	float _Vnp = ((kCoR + 1.0)*self.m*Vn + Vnp*(p.m - kCoR*self.m))/(self.m + p.m);
	// Store the results
	self.vN = _Vn;
	p.vN = _Vnp;
	// Now figure out what the final velocities are in the x, y coordinate system.
	float cos__phi = cos_phi;
	float sin__phi = -sin_phi;
	float v = _Vn*cos__phi + Vt*sin__phi;
	velocity[0] = v;
	v = Vt*cos__phi - _Vn*sin__phi;
	velocity[1] = v;
	// Do the same for p
	v = _Vnp*cos__phi + Vtp*sin__phi;
	[p setVx:v];
	v = Vtp*cos__phi - _Vnp*sin__phi;
	[p setVy:v];
	// NSLog(@"Velocities after collision V1x = %f, V1y = %f, V2x = %f, V2y = %f", velocity[0], velocity[1], [p getVx], [p getVy]);
}

/**
 * Reset anything which is required of the particle before
 * doing something else.
 */
- (void)reset {
	self.accPx = 0.0f;
	self.accPy = 0.0f;
	self.accKx = 0.0f;
	self.accKy = 0.0f;
	if (self.gravity != HYStateWeakZero) {
		self.gravity = HYWeakPlus;
	}
}

/**
 * Handle the strong interaction with other particles. These
 * effects accumulate with each particle. Also handle penetration
 * with a spring interaction.
 */
- (void)interactWithParticle:(Particle *)p {
	// Handle penetration first.
	float dx = p.x - self.x;
	float dy = p.y - self.y;
	float d = hypot(dx, dy);
	if (d < kRestoreDistanceFactor * (p.r + self.r))  {
		// Energy loss from collison.
		[self absorb:kAbsorption];
		[p absorb:kAbsorption];
		// Penetration has occurred. Apply a spring restoring force to 
		// the particles at low velocity.
		double phi = atan((p.y - self.y)/(p.x - self.x));
		float dr = kRestoreDistanceFactor * (self.r + p.r) - d; // Amount of compression.
		float a = kSpring * dr / p.m; // Acceleration from the spring.
		float ax = fabs(a * cos(phi));
		float ay = fabs(a * sin(phi));
		// Fix the signs.
		ax = p.x > self.x ? -ax : ax;
		ay = p.y > self.y ? -ay : ay;
		if (fabs(ax) > kMaxRestore) {
			ax = ax < 0 ? -kMaxRestore : kMaxRestore;
		}
		if (fabs(ay) > kMaxRestore) {
			ay = ay < 0 ? -kMaxRestore : kMaxRestore;
		}		
		self.accKx += ax;
		self.accKy += ay;
	}	

	if (self.charge == HYStrongZero || p.charge == HYStrongZero || self.discharged || p.discharged) {
		// No other interactions.
		return;
	}
	// If the charges are the same then there is repulsion.
	BOOL repulsion = (self.charge == p.charge);
	// The interaction is like gravity k/(R*R)
	float ax = kE * dx / (d*d*d);
	float ay = kE * dy / (d*d*d);
	if (repulsion) {
		ax = -ax;
		ay = -ay;
	}
	self.accPx += ax;
	self.accPy += ay;
	if (fabs(self.accPx) > kMaxStrongInteraction) {
		self.accPx = self.accPx < 0 ? -kMaxStrongInteraction : kMaxStrongInteraction;
	}
	if (fabs(self.accPy) > kMaxStrongInteraction) {
		self.accPy = self.accPy < 0 ? -kMaxStrongInteraction : kMaxStrongInteraction;
	}	
}

#pragma mark -
#pragma mark === Rendering ===

- (void)fade
{
	if (self.opacity > kMinimumOpacity) {
		self.opacity -= 0.1;
	}
}

- (void)drop
{
	if (self.scale.x > kMinimumScale) {
		float s = self.scale.x;
		s -= 0.1;
		self.scale = CGPointMake(s, s);
	}
}

- (BOOL)isDropped
{
	float s = self.scale.x;
	return s <= kMinimumScale;
}

- (BOOL)isNormalSize
{
	float s = self.scale.x;
	return s == 1.0f;
}

@end
