//
//  Tile.h
//  Hydrogen
//
//  Created by Albert on 1/26/09.
//  Copyright 2009 Whifflebird. All rights reserved.
//

#import  <Cocoa/Cocoa.h>

// All the tile types. The father knows his children.

/*
 * These values are on the map. The first 6 bytes represent tiles.
 * The first two bytes represent tiles that can be overlayed. The
 * next two bytes are tiles that cannot be overlayed and the last two
 * bytes represent tiles that can move.
 * The next 4 bytes are particles. 
 */
enum {
	/* Floor tiles. */
	HYFloor						= 0x00000000, 
	HYWater						= 0x00000001,
	HYAbyss						= 0x00000002,
	HYGrayFloor					= 0x00000003,
	HYWoodFlHor					= 0x00000004,
	HYWoodFlVert				= 0x00000005,
	HYWoodPanelA				= 0x00000006,
	HYWoodPanelB				= 0x00000007,
	HYWoodPanelC				= 0x00000008,
	HYMarbleFlA					= 0x00000009,
	HYMarbleFlB					= 0x0000000A,
	HYMarbleFlC					= 0x0000000B,
	HYMarbleFlD					= 0x0000000C,
	HYTrapFlHorOpen				= 0x0000000D,
	HYTrapFlHorClosed			= 0x0000000E,
	HYTrapFlVertOpen			= 0x0000000F,
	HYTrapFlVertClosed			= 0x00000010,
	HYSpace						= 0x00000011,
	HYDirt						= 0x00000012,
	HYGrass						= 0x00000013,
	HYGravel					= 0x00000014,
	HYSand						= 0x00000015,
	HYTrapTransporterTA			= 0x00000016,
	HYTrapTransporterTB			= 0x00000017,
	HYTrapTransporterTC			= 0x00000018,
	HYTrapTransporterTD			= 0x00000019,
	HYTrapTransporterEA			= 0x0000001A,
	HYTrapTransporterEB			= 0x0000001B,
	HYTrapTransporterEC			= 0x0000001C,
	HYTrapTransporterED			= 0x0000001D,
	HYWormholeTransporterTA		= 0x0000001E,
	HYWormholeTransporterTB		= 0x0000001F,
	HYWormholeTransporterTC		= 0x00000020,
	HYWormholeTransporterTD		= 0x00000021,
	HYWormholeTransporterEA		= 0x00000022,
	HYWormholeTransporterEB		= 0x00000023,
	HYWormholeTransporterEC		= 0x00000024,
	HYWormholeTransporterED		= 0x00000025,
	/* Regular object tiles. */
	HYStone						= 0x00000100,
	HYCrater					= 0x00000200,
	HYHome						= 0x00000300,
	HYHomeProton				= 0x00000400,
	HYHomeElectron				= 0x00000500,
	HYStation					= 0x00000600,
	HYDischarger				= 0x00000700,
	HYCharger					= 0x00000800,
	HYTrap						= 0x00000900,
	HYMessageTile				= 0x00000A00,
	HYSwitchOn					= 0x00000B00,
	HYSwitchOff					= 0x00000C00,
	HYStoneA					= 0x00000D00,
	HYStoneB					= 0x00000E00,
	HYStoneC					= 0x00000F00,
	HYWormhole					= 0x00001000,
	HYElectricMonoPole			= 0x00001100,
	HYPositiveMonoPoleOn		= 0x00001200,
	HYPositiveMonoPoleOff		= 0x00001300,
	HYNegativeMonoPoleOn		= 0x00001400,
	HYNegativeMonoPoleOff		= 0x00001500,
	HYMine						= 0x00001600,
	HYSilver					= 0x00001700,
	HYGold						= 0x00001800,
	HYPin						= 0x00001900,
	HYRedPin					= 0x00001A00,
	HYGreenPin					= 0x00001B00,
	HYYellowPin					= 0x00001C00,
	HYBomb						= 0x00001D00,
	HYMegaBomb					= 0x00001E00,
	HYTurquoise					= 0x00001F00,
	HYRuby						= 0x00002000,
	HYBluePin					= 0x00002100,
	HYInvisibility				= 0x00002200,
	HYShield					= 0x00002300,
	HYColumn					= 0x00002400,
    HYUmbrella                  = 0x00002500,
    HYRepulsion                 = 0x00002600,
	/* Moveable tiles or tiles involved in a move. */
	HYWood						= 0x00010000,
	HYWoodFloor					= 0x00020000,
	HYDetonator					= 0x00030000,
	HYExplosiveCharge			= 0x00040000,
	/* Tents */
	HYForceFieldOn				= 0x00100000,
	HYForceFieldOff				= 0x00200000,
	/* Masks */
	HYFloorOnlyMask				= 0x000000FF,
	HYTileOnlyMask				= 0x0000FF00,
	HYMoveableOnly				= 0x000F0000,
	HYFloorMask					= 0xFF0000FF,
	HYTileMask					= 0xFF00FF00,
	HYMoveableMask				= 0xFF0F0000,
	HYNotFloorMask				= 0xFFFFFF00,
	HYParticleMask				= 0xFF000000,
	HYNotParticle				= 0x00FFFFFF,
	HYTentMask					= 0x00F00000,
	HYNotTent					= 0xFF0FFFFF
};

// Tile moves. Tiles can only move from one tile square to another.
enum {
	HYMoveStay = 0,
	HYMoveLeft = 1,
	HYMoveRight = 2,
	HYMoveUp = 3,
	HYMoveDown = 4
};

// Tile interactions.
enum {
	HYBlock = 0, // Tile blocked by static tile on move
	HYOverlay = 1, // Tile covers static tile on move - essentially just moves
	HYWoodTransform = 2 // Tile is transformed on move by static tile
};

// Tile states.
enum {
	HYNormal					= 0,
	HYTransformToWoodFlPending	= 1,
	HYSwitchedOn				= 2,
	HYSwitchedOff				= 3,
	HYSwitchedOpening			= 4,
	HYSwitchedClosing			= 5,
	HYTransfer					= 6,
	HYExpel						= 7,
	HYRemove					= 8,
	HYExplode					= 9,
	HYBombInActive				= 10,
	HYBombActivationStart		= 11,
	HYBombActivation			= 12,
	HYBombCountDown				= 13,
	HYBombFlash					= 14,
	HYBombCountDownExplosion	= 15,
	HYBombExplosion				= 16
};

// Tile levels.
enum {
	HYFloorLevel				= 0,
	HYOnFloor					= 1,
	HYOnFloorMove				= 2,
	HYStoneLevel				= 3,
	HYOnStone					= 4
};

@class Particle;
@class Texture2D;

@interface Tile : NSObject

@property (assign, nonatomic) float x; // Location of the center of the tile
@property (assign, nonatomic) float y; // Location of the center of the tile
@property (assign, nonatomic) float f; // Friction of tile
@property (assign, nonatomic) int row; // Row in the map
@property (assign, nonatomic) int column; // Column in the map
@property (assign, nonatomic) int moveRequest; // Direction the tile wants to go
@property (assign, nonatomic) int type; // Type of tile
@property (assign, nonatomic) int state; // State the tile is in
@property (assign, nonatomic) float collisionSpeed; // The normal velocity of the particle hitting the tile
@property (assign, nonatomic) float sequence;
@property (assign, nonatomic) int action; // What the tile is doing.
@property (assign, nonatomic) BOOL top; // Draw the tile at the top layer.
@property (assign, nonatomic) int tileID; // The id of the tile
@property (assign, nonatomic) int viewLevel; // The view level of the tile. Is it at the floor level? Is it on the floor?
@property (strong, nonatomic) Texture2D *image;
@property (strong, nonatomic) NSArray *imageSequence;
@property (assign, nonatomic) float opacity;
@property (assign, nonatomic) float rotation;
@property (assign, nonatomic) CGPoint scale;

+ (int)addTileValue:(int)oldValue withValue:(int)newValue;
+ (BOOL)isSourceValue:(int)value;
+ (BOOL)isTargetValue:(int)value;
+ (BOOL)isBlockable:(int)value;
- (BOOL)isParticleInside:(Particle *)p;
- (BOOL)isParticleAtCenter:(Particle *)p;
- (float)distanceFromCenter:(Particle *)p;
- (void)updatePositionInsideWithParticle:(Particle *)p AccX:(float)accX AccY:(float)accY AccZ:(float)accZ;
- (int)getSequence;
- (void)resetSequence;

@end
