//
//  HYLevel.h
//  HYLevelMaker
// 
//  A freeze dried version of Map.
//
//  Created by Albert on 4/7/09.
//  Copyright 2009 Whifflebird. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kMaxColumns				9
#define kMaxRows				13
#define kNameKey				@"Name"
#define kClueKey				@"Clue"
#define kMapKey					@"Map"
#define kBonusKey				@"Bonus"
#define kFirstLevel				@"FirstLevel"
#define kNextLevel				@"NextLevel"
#define kRoomA					@"RoomA"
#define kRoomB					@"RoomB"
#define kRoomC					@"RoomC"
#define kRoomD					@"RoomD"
#define kSnapShot				@"SnapShot"
#define kConnections			@"Connections"
#define kStartLevelOfMap		@"StartLevelOfMap"
#define kIsFreeMap				@"isFreeMap"

enum {
	HYRoomA		= 0,
	HYRoomB		= 1,
	HYRoomC		= 2,
	HYRoomD		= 3
};

@interface HYLevel : NSObject <NSCoding, NSCopying>

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *clue;
@property (strong, nonatomic) NSMutableArray *map;
@property (assign, nonatomic) int bonus;
@property (strong, nonatomic) NSString *firstLevel;
@property (strong, nonatomic) NSString *nextLevel;
@property (strong, nonatomic) NSString *roomA;
@property (strong, nonatomic) NSString *roomB;
@property (strong, nonatomic) NSString *roomC;
@property (strong, nonatomic) NSString *roomD;
@property (strong, nonatomic) NSData *snapShot;
@property (strong, nonatomic) NSMutableArray *connections;
@property (assign, nonatomic) BOOL startLevelOfMap;
@property (assign, nonatomic) BOOL isFreeMap;

+ (HYLevel *)getLevelAtPath:(NSString *)path;
- (HYLevel *)getFirstLevel;
- (NSString *)findLevelNameAt:(int)requestedLevel;
- (HYLevel *)findLevelAt:(int)requestedLevel;
+ (HYLevel *)getMapWithName:(NSString *)name;
- (HYLevel *)getNextLevel;
- (HYLevel *)getRoom:(int)room;
- (int)getNumberOfLevels;

@end
