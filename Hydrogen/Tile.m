//
//  Tile.m
//  Hydrogen
//
//  Created by Albert on 1/26/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "Tile.h"
#import "Particle.h"

@implementation Tile

#define kNearCenter				5.0

+ (int)addTileValue:(int)oldValue withValue:(int)newValue {
	if (newValue & HYFloorOnlyMask) {
		// The new value is a floor type.
		int d = HYNotFloorMask & oldValue; // Keep the other values value.
		// Put in the new floor value.
		return d | newValue;
	} else if (newValue & HYTileOnlyMask) {
		// The new value is a tile type.
		int d;
		switch (newValue) {
			case HYDischarger:
			case HYCharger:
			case HYTurquoise:
			case HYRuby:
			case HYGold:
			case HYSilver:
			case HYInvisibility:
            case HYUmbrella:
            case HYRepulsion:
			case HYShield:
			case HYMessageTile:
			case HYPositiveMonoPoleOn:
			case HYPositiveMonoPoleOff:
			case HYNegativeMonoPoleOn:
			case HYNegativeMonoPoleOff:
			case HYMine:
			case HYRedPin:
			case HYGreenPin:
			case HYYellowPin:
			case HYBluePin:
			case HYBomb:
			case HYExplosiveCharge:
			case HYMegaBomb:
				// These tiles show parts of the floor.
				d = HYFloorMask & oldValue; // Keep the floor value and the particle.
				// Put in the new tile type value.
				return d | newValue;				
			default:
				return newValue;
		}
	} else if (newValue & HYMoveableOnly) {
		int d = HYFloorMask & oldValue; // Keep the floor and the particle.
		// Put in the new moveable tile value.
		return d | newValue;
	} else if (newValue & HYParticleMask) {
		int d = HYNotParticle & oldValue; // Keep everything but the particle.
		// Put in the new particle value.
		return d | newValue;
	} else if (newValue & HYTentMask) {
		int d = HYNotTent & oldValue; // Keep everything but the tent.
		return d | newValue;
	} else {
		return HYFloor;
	}
}

/*
 * Tiles that can target other tiles. The worm hole can target another
 * worm hole as a destination.
 */
+ (BOOL)isSourceValue:(int)value {
	if ((value & HYTileOnlyMask) == HYSwitchOn || (value & HYTileOnlyMask) == HYSwitchOff ||
		(value & HYTileOnlyMask) == HYWormhole) {
		return YES;
	}
	return NO;
}

/*
 * Tiles that can be targeted by switches.
 */
+ (BOOL)isTargetValue:(int)value {
	if ((value & HYTentMask) == HYForceFieldOn || (value & HYTentMask) == HYForceFieldOff ||
		(value & HYFloorOnlyMask) == HYTrapFlHorOpen || (value & HYFloorOnlyMask) == HYTrapFlHorClosed ||
		(value & HYFloorOnlyMask) == HYTrapFlVertOpen || (value & HYFloorOnlyMask) == HYTrapFlVertClosed ||
		(value & HYTileOnlyMask) == HYWormhole || (value & HYTileOnlyMask) == HYPositiveMonoPoleOn ||
		(value & HYTileOnlyMask) == HYPositiveMonoPoleOff || (value & HYTileOnlyMask) == HYNegativeMonoPoleOn ||
		(value & HYTileOnlyMask) == HYNegativeMonoPoleOff) {
		return YES;
	}
	return NO;
}

/**
 * Tiles that block the sides of other tiles.
 */
+ (BOOL)isBlockable:(int)value {
	if (value == HYStone || value == HYSwitchOn ||
		value == HYSwitchOff || value == HYStoneA ||
		value == HYStoneB || value == HYStoneC) {
		return YES;
	} else {
		return NO;
	}
}

- (instancetype)init {
	self = [super init];
	if (self) {
        self.image = nil;
		self.f = 1.0; // No friction
		self.moveRequest = HYMoveStay;
		self.type = HYFloor;
		self.state = HYNormal;
		self.collisionSpeed = 0.0f;
		self.sequence = 0.0f;
		self.viewLevel = HYFloorLevel;
		self.opacity = 1.0f;
		self.rotation = 0.0f;
		self.scale = CGPointMake(1.0f, 1.0f);
	}
	return self;
}

- (BOOL)isParticleInside:(Particle *)p {
	return NO;
}

- (BOOL)isParticleAtCenter:(Particle *)p {
	// Get the distance of the particle from the center.
	float s = hypot(p.x - self.x, p.y - self.y);
	return (s < kNearCenter);
}

- (void)updatePositionInsideWithParticle:(Particle *)p AccX:(float)accX AccY:(float)accY AccZ:(float)accZ {
	return;
}

- (float)distanceFromCenter:(Particle *)p {
	return hypot(p.x - self.x, p.y - self.y);
}

- (int)getSequence {
	int n;
	int m = (int)self.sequence++;
	n = m / 2;
	return n;
}

- (void)resetSequence {
	self.sequence = 0.0f;
}

@end
