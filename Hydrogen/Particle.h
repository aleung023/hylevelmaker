//
//  Particle.h
//  Hydrogen
//
//  Created by Albert on 1/13/09.
//  Copyright 2009 Whifflebird. All rights reserved.
//

#import  <Cocoa/Cocoa.h>

@class Texture2D;

#define kFilmLength					10

/**
 * Particle types. These values are on the map. The first 4 bytes represent tiles.
 * The next 4 bytes are particles.
 */
enum {
	HYElectron			= 0x01000000,
	HYElectronCharged	= 0x02000000,
	HYProton			= 0x03000000,
	HYProtonCharged		= 0x04000000,
	HYChaser			= 0x05000000,
	HYCone				= 0x06000000,
	HYCan				= 0x07000000,
	HYSpinner1			= 0x08000000,
	HYSpinner2			= 0x09000000,
};

/**
 * Interaction types
 */
enum  {
	HYWeakZero			= 0x00000000, // No interaction with gravity
	HYWeakPlus			= 0x00000001, // Gravity in the normal direction
	HYWeakMinus			= 0x00000002, // Gravity in the opposite direction
	HYStrongZero		= 0x00000004, // No interaction with other particles
	HYStrongPlus		= 0x00000008, // Positive charge
	HYStrongMinus		= 0x00000010, // Negative charge
	HYStateWeakZero		= 0x00000020, // The particle cannot interact with gravity at all. This state cannot be changed.
};

/**
 * States
 */
enum {
	HYStable			= 0x00000000, // Stable, nothing effecting it
	HYKill				= 0x00000001, // Particle should be removed
	HYDrowning			= 0x00000002, // Fell in the water and about to disappear
	HYFalling			= 0x00000004, // Falling into the abyss
	HYTransporting		= 0x00000008, // Falling into the worm hole 
	HYExiting			= 0x00000010, // Coming out of the worm hole
	HYShattered			= 0x00000020, // Particle is broken
	HYBreaking			= 0x00000040, // Particle is breaking apart
	HYWarping			= 0x00000080, // Partilce is moving to new room
	HYTunnelling		= 0x00000100, // Particle is tunnelling to new room using underground passage way.
	HYCharmed			= 0x00000200, // Particle is charmed
	HYLeveling1			= 0x00000400, // Particle is leveling out
	HYLeveling2			= 0x00000800, // Particle is leveling out for good
};

@class Wormhole;

@interface Particle : NSObject

@property (assign, nonatomic) int particleID; // The ID of the particle
@property (assign, nonatomic) float r; // radius of particle
@property (assign, nonatomic) float timeInterval; // time interval between acceleration values
@property (assign, nonatomic) float x; // x position
@property (assign, nonatomic) float y; // y position
@property (assign, nonatomic) float m; // mass of particle
@property (assign, nonatomic) float vN; // velocity in the normal direction of collision
@property (assign, nonatomic) float f; // friction loss
@property (assign, nonatomic) int type; // the type of particle
@property (assign, nonatomic) int charge; // Strong interaction
@property (assign, nonatomic) int gravity; // Weak interaction
@property (assign, nonatomic) int state; // the state of the particle
@property (assign, nonatomic) int charm; // Particle treasure points.
@property (assign, nonatomic) int shields; // The amount of armor against attacks from chasers.
@property (assign, nonatomic) int sequence; // general counter for the particle
@property (assign, nonatomic) BOOL discharged; // Particle has lost charge
@property (assign, nonatomic) BOOL massless; // Particle has lost mass
@property (strong, nonatomic) Wormhole *warpDestination;
@property (strong, nonatomic) NSString *roomDestination;
@property (strong, nonatomic) Texture2D *image;
@property (strong, nonatomic) Texture2D *shatteredImage;
@property (strong, nonatomic) NSArray *imageSequence;
@property (strong, nonatomic) NSArray *drownSequence;
@property (strong, nonatomic) NSArray *fallingSequence;
@property (strong, nonatomic) NSArray *breakSequence;
@property (strong, nonatomic) NSArray *charmSequence;
@property (assign, nonatomic) float opacity;
@property (assign, nonatomic) float rotation;
@property (assign, nonatomic) CGPoint scale;
@property (assign, nonatomic) BOOL invisible;
@property (assign, nonatomic) float accGx; // force from gravity
@property (assign, nonatomic) float accGy; // force from gravity
@property (assign, nonatomic) float accPx; // force from particle interactions
@property (assign, nonatomic) float accPy; // force from particle interactions
@property (assign, nonatomic) float accKx; // restoring force from collisions.
@property (assign, nonatomic) float accKy; // restoring force from collisions.


- (float)distanceFromParticle:(Particle *)p;
- (void)updatePositionWithAccX:(float)accX AccY:(float)accY Qx:(float)qX Qy:(float)qY Kx:(float)kX Ky:(float)kY;
- (void)reflectVx;
- (void)reflectVy;
- (void)reflectVxNoLoss;
- (void)reflectVyNoLoss;
- (float)getVx;
- (void)setVx:(float)v;
- (float)getVy;
- (void)setVy:(float)v;
- (float)Ex;
- (float)Ey;
- (BOOL)determineCollisionWithParticle:(Particle *)p;
- (void)handleCollisionWithParticle:(Particle *)p;
- (float)speed;
- (float)energy;
- (void)reset;
- (void)interactWithParticle:(Particle *)p;
- (void)absorb:(float)p;
- (void)fade;
- (void)drop;
- (BOOL)isDropped;
- (BOOL)isNormalSize;

@end
