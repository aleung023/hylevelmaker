//
//  HYLevel.m
//  HYLevelMaker
//
//  Created by Albert on 4/7/09.
//  Copyright 2009 Whifflebird. All rights reserved.
//

#import "HYLevel.h"

#define kLastLevel			@"."

@implementation HYLevel

#pragma mark -
#pragma mark === Level Navigation ===
#pragma mark -
+ (HYLevel *)getLevelAtPath:(NSString *)path {
    NSError *error;
    NSData *data = [NSData dataWithContentsOfFile:path options:0 error:&error];
    if (error) {
        NSLog(@"error %@", error.localizedDescription);
        NSLog(@"error %@", error.localizedFailureReason);
    }
    NSAssert(data != nil, @"data cannot be nil at this point");
	HYLevel *level;
    level = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return level;
}

- (HYLevel *)getFirstLevel {
	NSString *levelPath = [[NSBundle mainBundle] pathForResource:self.firstLevel ofType:@"hylv"];
    NSError *error;
    NSData *data = [NSData dataWithContentsOfFile:levelPath options:0 error:&error];
    if (error) {
        NSLog(@"error %@", error.localizedDescription);
        NSLog(@"error %@", error.localizedFailureReason);
    }
    NSAssert(data != nil, @"data cannot be nil at this point");
	HYLevel *level;
    level = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return level;
}

- (NSString *)findLevelNameAt:(int)requestedLevel {
	// Start here.
	NSString *levelName = self.firstLevel;
	NSString *requestedLevelName = nil;
	HYLevel *myLevel;
	// Go through the level pack until the 
	// correct level is reached.
	for (int i = 0; i < (requestedLevel + 1); i++) {
		if ([levelName isEqualToString:kLastLevel]) {
			return nil;
		}
		NSString *levelPath = [[NSBundle mainBundle] pathForResource:levelName ofType:@"hylv"];
        NSError *error;
        NSData *data = [NSData dataWithContentsOfFile:levelPath options:0 error:&error];
        if (error) {
            NSLog(@"error %@", error.localizedDescription);
            NSLog(@"error %@", error.localizedFailureReason);
        }
        NSAssert(data != nil, @"data cannot be nil at this point");
        requestedLevelName = levelName;
        myLevel = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        levelName = myLevel.nextLevel;
	}
	return requestedLevelName;
}

- (HYLevel *)findLevelAt:(int)requestedLevel {
	// Start here.
	NSString *levelName = self.firstLevel;
	HYLevel *myLevel;
	// Go through the level pack until the 
	// correct level is reached.
	for (int i = 0; i < (requestedLevel + 1); i++) {
		if ([levelName isEqualToString:kLastLevel]) {
			return nil;
		}
		NSString *levelPath = [[NSBundle mainBundle] pathForResource:levelName ofType:@"hylv"];
        NSError *error;
        NSData *data = [NSData dataWithContentsOfFile:levelPath options:0 error:&error];
        if (error) {
            NSLog(@"error %@", error.localizedDescription);
            NSLog(@"error %@", error.localizedFailureReason);
        }
        NSAssert(data != nil, @"data cannot be nil at this point");
        myLevel = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        levelName = myLevel.nextLevel;
	}
	return myLevel;
}

- (int)getNumberOfLevels
{
	// Start here.
	NSString *levelName = self.firstLevel;
	HYLevel *myLevel;
	// Go through the level pack until the 
	// we reach the last level.
	int i = 0;
	while (YES) {
		if ([levelName isEqualToString:kLastLevel]) {
			return i;
		}	
		NSString *levelPath = [[NSBundle mainBundle] pathForResource:levelName ofType:@"hylv"];
        NSError *error;
        NSData *data = [NSData dataWithContentsOfFile:levelPath options:0 error:&error];
        if (error) {
            NSLog(@"error %@", error.localizedDescription);
            NSLog(@"error %@", error.localizedFailureReason);
        }
        NSAssert(data != nil, @"data cannot be nil at this point");
		myLevel = [NSKeyedUnarchiver unarchiveObjectWithData:data];
		levelName = myLevel.nextLevel;
		i++;
		NSAssert(i < 51, @"map package cannot exceed 50 levels");
	}	
}

+ (HYLevel *)getMapWithName:(NSString *)name {
	HYLevel *map;
	NSString *levelPath = [[NSBundle mainBundle] pathForResource:name ofType:@"hylv"];
    NSError *error;
    NSData *data = [NSData dataWithContentsOfFile:levelPath options:0 error:&error];
    if (error) {
        NSLog(@"error %@", error.localizedDescription);
        NSLog(@"error %@", error.localizedFailureReason);
    }
    NSAssert(data != nil, @"data cannot be nil at this point");
    map = [NSKeyedUnarchiver unarchiveObjectWithData:data];
	return map;
}

- (HYLevel *)getNextLevel {
	HYLevel *next;
	NSString *levelName = self.nextLevel;
	if ([levelName isEqualToString:kLastLevel]) {
		return nil;
	} else {
		NSString *levelPath = [[NSBundle mainBundle] pathForResource:levelName ofType:@"hylv"];
        NSError *error;
        NSData *data = [NSData dataWithContentsOfFile:levelPath options:0 error:&error];
        if (error) {
            NSLog(@"error %@", error.localizedDescription);
            NSLog(@"error %@", error.localizedFailureReason);
        }
        NSAssert(data != nil, @"data cannot be nil at this point");
        next = [NSKeyedUnarchiver unarchiveObjectWithData:data];
	}
	return next;
}

- (HYLevel *)getRoom:(int)room {
	NSString *roomName;
	switch (room) {
		case HYRoomA:
			roomName = self.roomA;
			break;
		case HYRoomB:
			roomName = self.roomB;
			break;
		case HYRoomC:
			roomName = self.roomC;
			break;
		case HYRoomD:
			roomName = self.roomD;
			break;
		default:
			return nil;
			break;
	}
	return [HYLevel getMapWithName:roomName];
}

#pragma mark -
#pragma mark === NSCoding ===
#pragma mark -
- (void)encodeWithCoder:(NSCoder *)encoder {
	[encoder encodeObject:self.name forKey:kNameKey];
	[encoder encodeObject:self.clue forKey:kClueKey];
	[encoder encodeObject:self.map forKey:kMapKey];
	[encoder encodeInt:self.bonus forKey:kBonusKey];
	[encoder encodeObject:self.firstLevel forKey:kFirstLevel];
	[encoder encodeObject:self.nextLevel forKey:kNextLevel];
	[encoder encodeObject:self.roomA forKey:kRoomA];
	[encoder encodeObject:self.roomB forKey:kRoomB];
	[encoder encodeObject:self.roomC forKey:kRoomC];
	[encoder encodeObject:self.roomD forKey:kRoomD];
	[encoder encodeObject:self.snapShot forKey:kSnapShot];
	[encoder encodeObject:self.connections forKey:kConnections];
	[encoder encodeBool:self.startLevelOfMap forKey:kStartLevelOfMap];
	[encoder encodeBool:self.isFreeMap forKey:kIsFreeMap];
}

- (id)initWithCoder:(NSCoder *)decoder {
	if (self = [super init]) {
		self.name = [decoder decodeObjectForKey:kNameKey];
		self.clue = [decoder decodeObjectForKey:kClueKey];
		self.map = [decoder decodeObjectForKey:kMapKey];
		self.bonus = [decoder decodeIntForKey:kBonusKey];
		self.firstLevel = [decoder decodeObjectForKey:kFirstLevel];
		self.nextLevel = [decoder decodeObjectForKey:kNextLevel];
		self.roomA = [decoder decodeObjectForKey:kRoomA];
		self.roomB = [decoder decodeObjectForKey:kRoomB];
		self.roomC = [decoder decodeObjectForKey:kRoomC];
		self.roomD = [decoder decodeObjectForKey:kRoomD];
		self.snapShot = [decoder decodeObjectForKey:kSnapShot];
		self.connections = [decoder decodeObjectForKey:kConnections];
		self.startLevelOfMap = [decoder decodeBoolForKey:kStartLevelOfMap];
		self.isFreeMap = [decoder decodeBoolForKey:kIsFreeMap];
	}
	return self;
}

#pragma mark -
#pragma mark === NSCopying ===
#pragma mark -
- (id)copyWithZone:(NSZone *)zone {
	HYLevel *copy = [[[self class] allocWithZone:zone] init];
	copy.name = [[self name] copy];
	copy.clue = [[self clue] copy];
	copy.map = [[self map] copy];
	copy.bonus = self.bonus;
	copy.firstLevel = [[self firstLevel] copy];
	copy.nextLevel = [[self nextLevel] copy];
	copy.roomA = [[self roomA] copy];
	copy.roomB = [[self roomB] copy];
	copy.roomC = [[self roomC] copy];
	copy.roomD = [[self roomD] copy];
	copy.snapShot = [[self snapShot] copy];
	copy.connections = [[self connections] copy];
	copy.startLevelOfMap = self.startLevelOfMap;
	copy.isFreeMap = self.isFreeMap;
	return copy;
}

@end
