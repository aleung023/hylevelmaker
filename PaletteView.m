//
//  PaletteView.m
//  HydrogenLevelMaker
//
//  Created by Albert on 3/31/09.
//  Copyright 2009 Whifflebird. All rights reserved.
//

#import "PaletteView.h"
#import "Tile.h"
#import "Particle.h"
#import "HYPalette.h"
#import "HYImages.h"

#define kTileWidth		32.0
#define kBumpRight		38.0
#define kBumpDown		38.0

@interface PaletteView ()

@property (strong, nonatomic) NSMutableArray *palettes;
@property (strong, nonatomic) NSEvent *mouseDownEvent;
@property (strong, nonatomic) HYImages *imageStore;

- (void)addToPalettes:(int)value withRect:(NSRect)rect;

@end

@implementation PaletteView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.palettes = [[NSMutableArray alloc] init];
        // Initialization code here.
		NSRect tileRect[kPaletteSize];
		float startY = frame.size.height - kTileWidth;
		NSRect startingRect = NSMakeRect(0.0, startY, kTileWidth, kTileWidth);
		tileRect[0] = startingRect;
		int i;
		NSRect rectPosition = tileRect[0];
		for (i = 1; i < kPaletteSize; i++) {
			// Start moving to the right.
			rectPosition.origin.x += kBumpRight;
			if (NSMaxX(rectPosition) > NSMaxX(frame)) {
				rectPosition.origin.x = startingRect.origin.x;
				rectPosition.origin.y -= kBumpDown;
			}
			tileRect[i] = rectPosition;
		}
		// Get an image store
		self.imageStore = [[HYImages alloc] init];
		i = 0;
		[self addToPalettes:HYProton withRect:tileRect[i++]];
		[self addToPalettes:HYProtonCharged withRect:tileRect[i++]];
		[self addToPalettes:HYElectron withRect:tileRect[i++]];
		[self addToPalettes:HYElectronCharged withRect:tileRect[i++]];
		[self addToPalettes:HYGrayFloor withRect:tileRect[i++]];
		[self addToPalettes:HYWoodFlHor withRect:tileRect[i++]];
		[self addToPalettes:HYWoodFlVert withRect:tileRect[i++]];
		[self addToPalettes:HYWoodPanelA withRect:tileRect[i++]];
		[self addToPalettes:HYWoodPanelB withRect:tileRect[i++]];
		[self addToPalettes:HYWoodPanelC withRect:tileRect[i++]];
		[self addToPalettes:HYMarbleFlA withRect:tileRect[i++]];
		[self addToPalettes:HYMarbleFlB withRect:tileRect[i++]];
		[self addToPalettes:HYMarbleFlC withRect:tileRect[i++]];
		[self addToPalettes:HYMarbleFlD withRect:tileRect[i++]];
		[self addToPalettes:HYDirt withRect:tileRect[i++]];
		[self addToPalettes:HYGrass withRect:tileRect[i++]];
		[self addToPalettes:HYGravel withRect:tileRect[i++]];
		[self addToPalettes:HYSand withRect:tileRect[i++]];
		[self addToPalettes:HYStone withRect:tileRect[i++]];
		[self addToPalettes:HYStoneA withRect:tileRect[i++]];
		[self addToPalettes:HYStoneB withRect:tileRect[i++]];
		[self addToPalettes:HYStoneC withRect:tileRect[i++]];
		[self addToPalettes:HYAbyss withRect:tileRect[i++]];
		[self addToPalettes:HYWater withRect:tileRect[i++]];
		[self addToPalettes:HYSpace withRect:tileRect[i++]];
		[self addToPalettes:HYCrater withRect:tileRect[i++]];
		[self addToPalettes:HYHomeProton withRect:tileRect[i++]];
		[self addToPalettes:HYHomeElectron withRect:tileRect[i++]];
		[self addToPalettes:HYHome withRect:tileRect[i++]];
		[self addToPalettes:HYSwitchOn withRect:tileRect[i++]];
		[self addToPalettes:HYSwitchOff withRect:tileRect[i++]];
		[self addToPalettes:HYCharger withRect:tileRect[i++]];
		[self addToPalettes:HYDischarger withRect:tileRect[i++]];
		[self addToPalettes:HYMessageTile withRect:tileRect[i++]];
		[self addToPalettes:HYWood withRect:tileRect[i++]];
		[self addToPalettes:HYForceFieldOn withRect:tileRect[i++]];
		[self addToPalettes:HYForceFieldOff withRect:tileRect[i++]];
		[self addToPalettes:HYTrapFlHorOpen withRect:tileRect[i++]];
		[self addToPalettes:HYTrapFlHorClosed withRect:tileRect[i++]];
		[self addToPalettes:HYTrapFlVertOpen withRect:tileRect[i++]];
		[self addToPalettes:HYTrapFlVertClosed withRect:tileRect[i++]];
		[self addToPalettes:HYWormhole withRect:tileRect[i++]];
		[self addToPalettes:HYWormholeTransporterTA withRect:tileRect[i++]];
		[self addToPalettes:HYWormholeTransporterTB withRect:tileRect[i++]];
		[self addToPalettes:HYWormholeTransporterTC withRect:tileRect[i++]];
		[self addToPalettes:HYWormholeTransporterTD withRect:tileRect[i++]];
		[self addToPalettes:HYWormholeTransporterEA withRect:tileRect[i++]];
		[self addToPalettes:HYWormholeTransporterEB withRect:tileRect[i++]];
		[self addToPalettes:HYWormholeTransporterEC withRect:tileRect[i++]];
		[self addToPalettes:HYWormholeTransporterED withRect:tileRect[i++]];
		[self addToPalettes:HYTrapTransporterTA withRect:tileRect[i++]];
		[self addToPalettes:HYTrapTransporterTB withRect:tileRect[i++]];
		[self addToPalettes:HYTrapTransporterTC withRect:tileRect[i++]];
		[self addToPalettes:HYTrapTransporterTD withRect:tileRect[i++]];
		[self addToPalettes:HYTrapTransporterEA withRect:tileRect[i++]];
		[self addToPalettes:HYTrapTransporterEB withRect:tileRect[i++]];
		[self addToPalettes:HYTrapTransporterEC withRect:tileRect[i++]];
		[self addToPalettes:HYTrapTransporterED withRect:tileRect[i++]];		
		[self addToPalettes:HYPositiveMonoPoleOn withRect:tileRect[i++]];
		[self addToPalettes:HYPositiveMonoPoleOff withRect:tileRect[i++]];
		[self addToPalettes:HYNegativeMonoPoleOn withRect:tileRect[i++]];
		[self addToPalettes:HYNegativeMonoPoleOff withRect:tileRect[i++]];
		[self addToPalettes:HYMine withRect:tileRect[i++]];
		[self addToPalettes:HYCone withRect:tileRect[i++]];
		[self addToPalettes:HYCan withRect:tileRect[i++]];
		[self addToPalettes:HYTurquoise withRect:tileRect[i++]];
		[self addToPalettes:HYRuby withRect:tileRect[i++]];
		[self addToPalettes:HYSilver withRect:tileRect[i++]];
		[self addToPalettes:HYGold withRect:tileRect[i++]];
		[self addToPalettes:HYInvisibility withRect:tileRect[i++]];
		[self addToPalettes:HYShield withRect:tileRect[i++]];
		[self addToPalettes:HYRedPin withRect:tileRect[i++]];
		[self addToPalettes:HYGreenPin withRect:tileRect[i++]];
		[self addToPalettes:HYYellowPin withRect:tileRect[i++]];
		[self addToPalettes:HYBluePin withRect:tileRect[i++]];
		[self addToPalettes:HYExplosiveCharge withRect:tileRect[i++]];
		[self addToPalettes:HYDetonator withRect:tileRect[i++]];
		[self addToPalettes:HYBomb withRect:tileRect[i++]];
		[self addToPalettes:HYMegaBomb withRect:tileRect[i++]];
		[self addToPalettes:HYSpinner1 withRect:tileRect[i++]];
		[self addToPalettes:HYSpinner2 withRect:tileRect[i++]];
        [self addToPalettes:HYUmbrella withRect:tileRect[i++]];
        [self addToPalettes:HYRepulsion withRect:tileRect[i++]];
	}
    return self;
}

- (void)addToPalettes:(int)value withRect:(NSRect)rect {
	HYPalette *p = [[HYPalette alloc] init];
	p.value = value;
	[self.imageStore assignImagesToTile:p];
	p.rect = rect;
	[self.palettes addObject:p];
}

- (void)drawRect:(NSRect)rect {
	for (HYPalette *pal in self.palettes) {
		if (pal.floorImage != nil) {
            [pal.floorImage drawInRect:pal.rect fromRect:NSZeroRect operation:NSCompositingOperationSourceOver fraction:1.0];
		} else if (pal.image != nil) {
            [pal.image drawInRect:pal.rect fromRect:NSZeroRect operation:NSCompositingOperationSourceOver fraction:1.0];
		} else {
            [pal.tentImage drawInRect:pal.rect fromRect:NSZeroRect operation:NSCompositingOperationSourceOver fraction:1.0];
		}
	}
}

#pragma mark -
#pragma mark === Drag and Drop ===
#pragma mark -
- (void)mouseDown:(NSEvent *)event {
	self.mouseDownEvent = event;
}

- (void)mouseDragged:(NSEvent *)event {
	// Figure out where the drag occurred.
	NSPoint down = [self.mouseDownEvent locationInWindow];
	NSPoint drag = [event locationInWindow];
	float distance = hypot(down.x - drag.x, down.y - drag.y);
	if (distance < 3) {
		return;
	}
	
	// Figure out which palette is being dragged.
	NSImage *image = nil;
	// Get the pasteboard
    NSPasteboard *pb = [NSPasteboard pasteboardWithName:NSPasteboardNameDrag];
	NSPoint p = [self convertPoint:down fromView:nil];
	for (HYPalette *palette in self.palettes) {
		if ([palette isPointInPallete:p]) {
			image = palette.dragImage;
			// Declare the type of data to write
            [pb declareTypes:[NSArray arrayWithObject:NSPasteboardTypeString] owner:self];
			// Copy the data to the paste board
            [pb setString:[[NSNumber numberWithInt:palette.value] stringValue] forType:NSPasteboardTypeString];
		}
	}
		
	// Start the drag
	if (image != nil) {
		// Drag at the center of the image.
		p.x = p.x - kTileWidth / 2.0;
		p.y = p.y - kTileWidth / 2.0;
		[self dragImage:image at:p offset:NSMakeSize(0.0, 0.0) event:self.mouseDownEvent pasteboard:pb source:self slideBack:YES];
	}
}

- (NSDragOperation)draggingSourceOperationMaskForLocal:(BOOL)isLocal {
	return NSDragOperationCopy;
}

- (void)draggedImage:(NSImage *)image endedAt:(NSPoint)screenPoint operation:(NSDragOperation)operation {
	return;
}

- (BOOL)acceptsFirstMouse:(NSEvent *)event {
    /*------------------------------------------------------
	 accept activation click as click in window
	 --------------------------------------------------------*/
    return YES; //so source doesn't have to be the active window
}

@end
