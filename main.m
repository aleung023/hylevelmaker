//
//  main.m
//  HYLevelMaker
//
//  Created by Albert on 4/5/09.
//  Copyright Whifflebird 2009 . All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **) argv);
}
