//
//  MyDocument.h
//  HYLevelMaker
//
//  Created by Albert on 4/5/09.
//  Copyright Whifflebird 2009 . All rights reserved.
//


#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import "Tile.h"
#import "LevelView.h"
#import "HYLevel.h"
#import "LevelTile.h"
#import "HYConnection.h"

@interface MyDocument : NSDocument

@end
