//
//  HYPalette.h
//  HydrogenLevelMaker
//
//  Created by Albert  Leung on 4/2/09.
//  Copyright 2009 WhiffleBird. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface HYPalette : NSObject

@property (strong, nonatomic) NSImage *image;
@property (strong, nonatomic) NSImage *floorImage;
@property (strong, nonatomic) NSImage *tentImage;
@property (strong, nonatomic) NSImage *dragImage;
@property (strong, nonatomic) NSImage *snapShotImage;
@property (assign, nonatomic) NSRect rect;
@property (assign, nonatomic) int value;

- (BOOL)isPointInPallete:(NSPoint)p;
- (NSPoint)getCenter;

@end
