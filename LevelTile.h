//
//  LevelTile.h
//  HydrogenLevelMaker
//
//  Created by Albert on 4/2/09.
//  Copyright 2009 Whifflebird. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "HYPalette.h"

@interface LevelTile : HYPalette

@property (assign, nonatomic) BOOL hilighted;
@property (assign, nonatomic) int index;

@end
