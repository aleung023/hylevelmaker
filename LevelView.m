//
//  LevelView.m
//  HydrogenLevelMaker
//
//  Created by Albert  Leung on 3/31/09.
//  Copyright 2009 WhiffleBird. All rights reserved.
//

#import "LevelView.h"
#import "LevelTile.h"
#import "HYImages.h"
#import "HYConnection.h"
#import "Particle.h"
#import "Tile.h"

@interface LevelView ()

@property (strong, nonatomic) LevelTile *draggedTile;
@property (strong, nonatomic) HYImages *imageStore;
@property (strong, nonatomic) NSEvent *mouseDownEvent;
@property (strong, nonatomic) NSBezierPath *connectionLine;
@property (assign, nonatomic) NSPoint startLine;
@property (assign, nonatomic) NSPoint endLine;
@property (strong, nonatomic) NSImage *pointerImage;
@property (assign, nonatomic) BOOL makeConnection;
@property (assign, nonatomic) BOOL fixSource;
@property (assign, nonatomic) BOOL fixTarget;
@property (assign, nonatomic) BOOL moveToSameLocation;

- (void)clear;
- (void)hiLightTileAtPoint:(NSPoint)p;
- (void)upDateTileAtPoint:(NSPoint)p withValue:(int)value;
- (void)connectToTileAtPoint:(NSPoint)p;
- (BOOL)isTileSource:(LevelTile *)tile;
- (BOOL)isTileTarget:(LevelTile *)tile;

@end

@implementation LevelView


#pragma mark -
#pragma mark === Initialization and Tearing down ===
#pragma mark -
- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
		self.doSnapShot = NO;
		// Get a image for the pointer
        self.pointerImage = [NSImage imageNamed:@"Pointer"];
        // Initialization code here.
		LevelTile *tile = [[LevelTile alloc] init];
		NSRect rectPosition = NSMakeRect(0.0, 0.0, kTileWidth, kTileWidth);
		NSRect startingRect = rectPosition;
		tile.image = nil;
		tile.rect = rectPosition;
		tile.value = 0;
		tile.index = 0;
        self.tiles = [[NSMutableArray alloc] init];
		[self.tiles addObject:tile];
		int i;
		for (i = 1; i < kNumberOfLevelTiles; i++) {
			// Start moving to the right.
			rectPosition.origin.x += kBump;
			if (NSMaxX(rectPosition) > NSMaxX(frame)) {
				// Move up.
				rectPosition.origin.x = startingRect.origin.x;
				rectPosition.origin.y += kBump;
			}
			tile = [[LevelTile alloc] init];
			tile.image = nil;
			tile.rect = rectPosition;
			tile.value = 0;
			tile.index = i;
			[self.tiles addObject:tile];
		}
		// Load the images
		self.imageStore = [[HYImages alloc] init];
		// Make this a drag destination.
		[self registerForDraggedTypes:[NSArray arrayWithObject:NSStringPboardType]];
		// Create a line that connects switches to devices.
		self.connectionLine = [[NSBezierPath alloc] init];
		[self.connectionLine setLineWidth:1.0];
		self.makeConnection = NO;
        self.fixSource = NO;
		self.fixTarget = NO;
		self.moveToSameLocation = NO;
		self.connections = [[NSMutableArray alloc] init];
		self.draggedTile = nil;
    }
    return self;
}

- (void)recreateConnections:(NSMutableArray *)pairValues {
	if (pairValues == nil || [pairValues count] == 0) {
		return;
	}
	int i = 0;
	for (i = 0; i < [pairValues count]; i += 2) {
		if ((i % 2) == 0) {
			HYConnection *conn = [[HYConnection alloc] init];
			NSNumber *s = [pairValues objectAtIndex:i];
			int k1 = [s intValue];
			NSNumber *t = [pairValues objectAtIndex:i + 1];
			int k2 = [t intValue];
			conn.source = [self.tiles objectAtIndex:k1];
			conn.target = [self.tiles objectAtIndex:k2];
			NSBezierPath *path = [[NSBezierPath alloc] init];
			[path setLineWidth:1.0];
			[path moveToPoint:[conn.source getCenter]];
			[path lineToPoint:[conn.target getCenter]];
			[path closePath];
			conn.connectionLine = path;
			[self.connections addObject:conn];
		}
	}
}

#pragma mark -
#pragma mark === Drawing ===
#pragma mark -
- (void)updateImagesToTiles {
	for (LevelTile *tile in self.tiles) {
		[self.imageStore assignImagesToTile:tile];
	}
}

- (void)drawRect:(NSRect)rect {
	if (self.doSnapShot) {
		// Draw the background image.
		[[self.imageStore getBackgroundImage] drawInRect:rect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
	}
	
	int i, j;
	for (i = 0; i < kMaxRows; i++) {
		for (j = 0; j < kMaxColumns; j++) {
			int k = kMaxColumns * i + j;
			LevelTile *tile = [self.tiles objectAtIndex:k];
			if (tile.value == HYFloor) {
				if (!self.doSnapShot) {
					// Draw checkerboard.
					if (k % 2 == 0) {
						[[NSColor lightGrayColor] set];
					} else {
						[[NSColor whiteColor] set];
					}	
					NSRectFill(tile.rect);	
				}
			} else {
				if (tile.floorImage != nil) {
					[tile.floorImage drawInRect:tile.rect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
				} else {
					if (!self.doSnapShot) {
						[[NSColor lightGrayColor] set];
						NSRectFill(tile.rect);						
					}
				}
				if (self.doSnapShot) {
					[tile.snapShotImage drawInRect:tile.rect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
				} else {
					[tile.image drawInRect:tile.rect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
				}
				if (tile.tentImage != nil) {
					if (self.doSnapShot) {
						[tile.snapShotImage drawInRect:tile.rect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
					} else {
						[tile.tentImage drawInRect:tile.rect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
					}
				}
			}
			if (tile.hilighted && !self.doSnapShot) {
				// Draw a gradient around the border.
				NSGradient *gr;
				gr = [[NSGradient alloc] initWithStartingColor:[NSColor whiteColor] endingColor:[NSColor grayColor]];
				[gr drawInRect:tile.rect relativeCenterPosition:NSZeroPoint];
			}
		}
	}	
	// Draw connection points
	if (!self.doSnapShot) {
		for (HYConnection *connection in self.connections) {
			[[NSColor cyanColor] set];
			[connection.connectionLine stroke];
		}
		if (self.makeConnection) {
			[[NSColor cyanColor] set];
			[self.connectionLine moveToPoint:self.startLine];
			[self.connectionLine lineToPoint:self.endLine];
			[self.connectionLine stroke];
		}		
	}
}

#pragma mark -
#pragma mark === Helper Methods for Drag and Drop ===
#pragma mark -
- (void)clear {
    for (LevelTile *tile in self.tiles) {
		tile.hilighted = NO;
	}
	[self setNeedsDisplay:YES];	
}

- (void)hiLightTileAtPoint:(NSPoint)p {
	// Do it under the cursor.
	p.x -= kTileWidth / 2.0;
	p.y -= kTileWidth / 2.0;
	for (LevelTile *tile in self.tiles) {
		tile.hilighted = [tile isPointInPallete:p];
	}
	[self setNeedsDisplay:YES];
}

- (void)upDateTileAtPoint:(NSPoint)p withValue:(int)value {
	// Do it under the cursor.
	p.x -= kTileWidth / 2.0;
	p.y -= kTileWidth / 2.0;
	for (LevelTile *tile in self.tiles) {
		if ([tile isPointInPallete:p]) {
			if (self.draggedTile != nil && self.draggedTile.index == tile.index) {
				// This is move to the same location.
				self.moveToSameLocation = YES;
				tile.hilighted = NO;			
				[self setNeedsDisplay:YES];			
				return;
			}
			int oldValue = tile.value;
			// Get rid of the hilight first.
			tile.value = HYFloor;
			tile.hilighted = NO;
			[self.imageStore assignImagesToTile:tile];
			[self setNeedsDisplay:YES];			
			// Combine the two values.
			tile.value = [Tile addTileValue:oldValue withValue:value];
			[self.imageStore assignImagesToTile:tile];
			if (self.fixSource || self.fixTarget) {
				for (HYConnection *connection in self.connections) {
					if (self.fixSource) {
						if (self.draggedTile.index == connection.source.index) {
							connection.source = tile;
							NSPoint anchorPoint = [connection.target getCenter];
							NSBezierPath *path = [[NSBezierPath alloc] init];
							[path setLineWidth:1.0];
							[path moveToPoint:anchorPoint];
							[path lineToPoint:[tile getCenter]];
							[path closePath];
							connection.connectionLine = path;
						}
					} else {
						if (self.draggedTile.index == connection.target.index) {
							connection.target = tile;
							NSPoint anchorPoint = [connection.source getCenter];
							NSBezierPath *path = [[NSBezierPath alloc] init];
							[path setLineWidth:1.0];
							[path moveToPoint:anchorPoint];
							[path lineToPoint:[tile getCenter]];
							[path closePath];
							connection.connectionLine = path;
						}
					}
				}
				self.fixSource = NO;
				self.fixTarget = NO;
			}
		}
	}
	[self setNeedsDisplay:YES];
}

- (void)connectToTileAtPoint:(NSPoint)p {
	for (LevelTile *tile in self.tiles) {
		if ([tile isPointInPallete:p]) {
			if ([Tile isTargetValue:tile.value]) {
				// Make the connection
				HYConnection *connection = [[HYConnection alloc] init];
				connection.source = self.draggedTile;
				connection.target = tile;
				NSBezierPath *path = [[NSBezierPath alloc] init];
				[path setLineWidth:1.0];
				[path moveToPoint:self.startLine];
				[path lineToPoint:[tile getCenter]];
				[path closePath];
				connection.connectionLine = path;
				[self.connections addObject:connection];
			}
		}
	}	
}

- (BOOL)isTileSource:(LevelTile *)tile {
	for (HYConnection *connection in self.connections) {
		if (tile.index == connection.source.index) {
			return YES;
		}
	}
	return NO;
}

- (BOOL)isTileTarget:(LevelTile *)tile {
	for (HYConnection *connection in self.connections) {
		if (tile.index == connection.target.index) {
			return YES;
		}
	}
	return NO;
}

#pragma mark -
#pragma mark === Dragging Source ===
#pragma mark -
- (void)mouseDown:(NSEvent *)event {
	self.mouseDownEvent = event;
}

- (void)mouseDragged:(NSEvent *)event {
	// Figure out where the drag occurred.
	NSPoint down = [self.mouseDownEvent locationInWindow];
	NSPoint drag = [event locationInWindow];
	float distance = hypot(down.x - drag.x, down.y - drag.y);
	if (distance < 3) {
		return;
	}
	
	// We can do a drag or a connect to tile from
	// a switch.
	BOOL control = NO;
	// Determine the control key was pressed.
	unsigned int flags;
	flags = [event modifierFlags];
	if (flags & NSControlKeyMask) {
		control = YES;
	} 
	// Figure out which tile is being dragged.
	NSImage *image = nil;
	// Get the pasteboard
	NSPasteboard *pb = [NSPasteboard pasteboardWithName:NSDragPboard];
	NSPoint p = [self convertPoint:down fromView:nil];
	for (LevelTile *tile in self.tiles) {
		if ([tile isPointInPallete:p]) {
			// We are in a tile.
			self.draggedTile = tile;
			if (!control) {
				if ([self isTileSource:tile]) {
					self.fixSource = YES;
				}
				if ([self isTileTarget:tile]) {
					self.fixTarget = YES;
				}
				image = tile.dragImage;
				// Declare the type of data to write
				[pb declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:self];
				// Copy the data to the paste board
				[pb setString:[[NSNumber numberWithInt:tile.value] stringValue] forType:NSStringPboardType];		
			} else {
				if ([Tile isSourceValue:tile.value]) {
					self.makeConnection = YES;
					NSPoint center = [tile getCenter];
					self.startLine = center;
					self.endLine = drag;
					self.connectionLine = [[NSBezierPath alloc] init];
					[self.connectionLine setLineWidth:1.0];
					image = self.pointerImage;
					[pb declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:self];
					// Copy the data to the paste board
					[pb setString:[[NSNumber numberWithInt:tile.index] stringValue] forType:NSStringPboardType];		
				}
			}
		}
	}
	// Start the drag
	if (image != nil) {
		// Drag at the center of the image.
		if (!self.makeConnection) {
			p.x = p.x - kTileWidth / 2.0;
			p.y = p.y - kTileWidth / 2.0;			
		} else {
			p.x -= 2.0;
			p.y -= 2.0;
		}
		[self dragImage:image at:p offset:NSMakeSize(0.0, 0.0) event:self.mouseDownEvent pasteboard:pb source:self slideBack:YES];
	}
}

- (NSDragOperation)draggingSourceOperationMaskForLocal:(BOOL)isLocal {
	return NSDragOperationMove | NSDragOperationCopy | NSDragOperationDelete;
}

/**
 * Called at the end of the drag.
 */
- (void)draggedImage:(NSImage *)image endedAt:(NSPoint)screenPoint operation:(NSDragOperation)operation {
	// Where did the image go?
	if (self.makeConnection) {
		self.makeConnection = NO;
		return;
	}
	int i;
	switch (operation) {
		case NSDragOperationMove:
			if (!self.moveToSameLocation) {
				self.draggedTile.value = HYFloor;
				self.draggedTile = nil;
				[self setNeedsDisplay:YES];
			} else {
				self.moveToSameLocation = NO;
			}
			break;
		case NSDragOperationCopy:
			break;
		case NSDragOperationDelete:
			// Did we delete a connection tile?
			for (i = 0; i < [self.connections count]; i++) {
				HYConnection *connection = [self.connections objectAtIndex:i];
				if (self.draggedTile.index == connection.source.index ||
					self.draggedTile.index == connection.target.index) {
					// Remove the connections before deallocting the connection.
					// The level tiles really don't get removed.
					connection.source = nil;
					connection.target = nil;
					[self.connections removeObjectAtIndex:i];
					break;					
				} 
			}
			self.draggedTile.value = HYFloor;
			self.draggedTile = nil;
			[self setNeedsDisplay:YES];
			break;
		default:
			break;
	}	
}

- (BOOL)acceptsFirstMouse:(NSEvent *)event {
    /*------------------------------------------------------
	 accept activation click as click in window
	 --------------------------------------------------------*/
    return YES; //so source doesn't have to be the active window
}

#pragma mark -
#pragma mark === Dragging Destination
#pragma mark -
- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender {
	if (!self.makeConnection) {
		NSPoint dragLocation = [sender draggingLocation];
		// Get the location in the view.
		dragLocation = [self convertPoint:dragLocation fromView:nil];
		[self hiLightTileAtPoint:dragLocation];		
	}
	if ([sender draggingSource] != self) {
		return NSDragOperationCopy;
	}
	NSDragOperation op = [sender draggingSourceOperationMask];
	if (op == NSDragOperationCopy) {
		// Option held down during drag.
		return NSDragOperationCopy;
	} else {
		return NSDragOperationMove;
	}
}

- (NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)sender {
	NSPoint dragLocation = [sender draggingLocation];
	// Get the location in the view.
	dragLocation = [self convertPoint:dragLocation fromView:nil];
	if (self.makeConnection) {
		// Draw the connection line
		self.endLine = dragLocation;
		self.connectionLine = [[NSBezierPath alloc] init];
		[self.connectionLine setLineWidth:1.0];
		[self setNeedsDisplay:YES];
	} else {
		[self hiLightTileAtPoint:dragLocation];
	}
	if ([sender draggingSource] != self) {
		return NSDragOperationCopy;
	}
	NSDragOperation op = [sender draggingSourceOperationMask];
	if (op == NSDragOperationCopy) {
		// Option held down during drag.
		return NSDragOperationCopy;
	} else {
		return NSDragOperationMove;
	}
}

- (void)draggingExited:(id <NSDraggingInfo>)sender {
	// Get rid of hilight.
	[self clear];
}

- (BOOL)prepareForDragOperation:(id <NSDraggingInfo>)sender {
	return YES;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender {
	NSPoint dragLocation = [sender draggingLocation];
	// Get the location in the view.
	dragLocation = [self convertPoint:dragLocation fromView:nil];
	if (!self.makeConnection) {
		NSPasteboard *pb = [sender draggingPasteboard];
		NSArray *types = [pb types];
		if ([types containsObject:NSStringPboardType]) {
			// Read the string from the pasteboard
			NSString *s = [pb stringForType:NSStringPboardType];
			int value = [s intValue];
			[self upDateTileAtPoint:dragLocation withValue:value];
			return YES;
		} else {
			return NO;
		}		
	} else {
		// Reset the flag at draggedImage
		[self connectToTileAtPoint:dragLocation];
		return YES;
	}
}

- (void)concludeDragOperation:(id <NSDraggingInfo>)sender {
	// Dont' do anything.
}
@end
