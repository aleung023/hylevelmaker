//
//  HYImages.m
//  HYLevelMaker
//
//  Created by Albert on 4/12/09.
//  Copyright 2009 Whifflebird. All rights reserved.
//

#import "HYImages.h"
#import "Particle.h"
#import "Tile.h"
#import "HYPalette.h"

@interface HYImages ()

@property (strong, nonatomic) NSImage *backgroundImage;
@property (strong, nonatomic) NSImage *electronImage;
@property (strong, nonatomic) NSImage *electronDragImage;
@property (strong, nonatomic) NSImage *electronChargedImage;
@property (strong, nonatomic) NSImage *electronChargedDragImage;
@property (strong, nonatomic) NSImage *protonImage;
@property (strong, nonatomic) NSImage *protonDragImage;
@property (strong, nonatomic) NSImage *protonChargedImage;
@property (strong, nonatomic) NSImage *protonChargedDragImage;
@property (strong, nonatomic) NSImage *abyssImage;
@property (strong, nonatomic) NSImage *abyssDragImage;
@property (strong, nonatomic) NSImage *chargeStationImage;
@property (strong, nonatomic) NSImage *chargeStationDragImage;
@property (strong, nonatomic) NSImage *craterImage;
@property (strong, nonatomic) NSImage *craterDragImage;
@property (strong, nonatomic) NSImage *homeProtonImage;
@property (strong, nonatomic) NSImage *homeProtonDragImage;
@property (strong, nonatomic) NSImage *homeElectronImage;
@property (strong, nonatomic) NSImage *homeElectronDragImage;
@property (strong, nonatomic) NSImage *homeImage;
@property (strong, nonatomic) NSImage *homeDragImage;
@property (strong, nonatomic) NSImage *greyFloorImage;
@property (strong, nonatomic) NSImage *greyFloorDragImage;
@property (strong, nonatomic) NSImage *woodFlTileHorImage;
@property (strong, nonatomic) NSImage *woodFlTileHorDragImage;
@property (strong, nonatomic) NSImage *woodFlTileVertImage;
@property (strong, nonatomic) NSImage *woodFlTileVertDragImage;
@property (strong, nonatomic) NSImage *woodPanelAImage;
@property (strong, nonatomic) NSImage *woodPanelADragImage;
@property (strong, nonatomic) NSImage *woodPanelBImage;
@property (strong, nonatomic) NSImage *woodPanelBDragImage;
@property (strong, nonatomic) NSImage *woodPanelCImage;
@property (strong, nonatomic) NSImage *woodPanelCDragImage;
@property (strong, nonatomic) NSImage *marbleFlAImage;
@property (strong, nonatomic) NSImage *marbleFlADragImage;
@property (strong, nonatomic) NSImage *marbleFlBImage;
@property (strong, nonatomic) NSImage *marbleFlBDragImage;
@property (strong, nonatomic) NSImage *marbleFlCImage;
@property (strong, nonatomic) NSImage *marbleFlCDragImage;
@property (strong, nonatomic) NSImage *marbleFlDImage;
@property (strong, nonatomic) NSImage *marbleFLDDragImage;
@property (strong, nonatomic) NSImage *groundStationImage;
@property (strong, nonatomic) NSImage *groundStationDragImage;
@property (strong, nonatomic) NSImage *messageTileImage;
@property (strong, nonatomic) NSImage *messageTileDragImage;
@property (strong, nonatomic) NSImage *turquoiseTileImage;
@property (strong, nonatomic) NSImage *turquoiseTileDragImage;
@property (strong, nonatomic) NSImage *rubyTileImage;
@property (strong, nonatomic) NSImage *rubyTileDragImage;
@property (strong, nonatomic) NSImage *silverTileImage;
@property (strong, nonatomic) NSImage *silverTileDragImage;
@property (strong, nonatomic) NSImage *goldTileImage;
@property (strong, nonatomic) NSImage *goldTileDragImage;
@property (strong, nonatomic) NSImage *invisibilityTileImage;
@property (strong, nonatomic) NSImage *invisibilityTileDragImage;
@property (strong, nonatomic) NSImage *shieldTileImage;
@property (strong, nonatomic) NSImage *shieldTileDragImage;
@property (strong, nonatomic) NSImage *stoneImage;
@property (strong, nonatomic) NSImage *stoneDragImage;
@property (strong, nonatomic) NSImage *stoneAImage;
@property (strong, nonatomic) NSImage *stoneADragImage;
@property (strong, nonatomic) NSImage *stoneBImage;
@property (strong, nonatomic) NSImage *stoneBDragImage;
@property (strong, nonatomic) NSImage *stoneCImage;
@property (strong, nonatomic) NSImage *stoneCDragImage;
@property (strong, nonatomic) NSImage *switchOnImage;
@property (strong, nonatomic) NSImage *switchOnDragImage;
@property (strong, nonatomic) NSImage *switchOffImage;
@property (strong, nonatomic) NSImage *switchOffDragImage;
@property (strong, nonatomic) NSImage *waterImage;
@property (strong, nonatomic) NSImage *waterDragImage;
@property (strong, nonatomic) NSImage *woodImage;
@property (strong, nonatomic) NSImage *woodDragImage;
@property (strong, nonatomic) NSImage *woodFlImage;
@property (strong, nonatomic) NSImage *woodFlDragImage;
@property (strong, nonatomic) NSImage *forceFieldOnImage;
@property (strong, nonatomic) NSImage *forceFieldOnDragImage;
@property (strong, nonatomic) NSImage *forceFieldOffImage;
@property (strong, nonatomic) NSImage *forceFieldOffDragImage;
@property (strong, nonatomic) NSImage *forceFieldImage;
@property (strong, nonatomic) NSImage *trapFlHorOpenImage;
@property (strong, nonatomic) NSImage *trapFlHorOpenDragImage;
@property (strong, nonatomic) NSImage *trapFlHorClosedImage;
@property (strong, nonatomic) NSImage *trapFlHorClosedDragImage;
@property (strong, nonatomic) NSImage *trapFlVertOpenImage;
@property (strong, nonatomic) NSImage *trapFlVertOpenDragImage;
@property (strong, nonatomic) NSImage *trapFlVertClosedImage;
@property (strong, nonatomic) NSImage *trapFlVertClosedDragImage;
@property (strong, nonatomic) NSImage *wormHoleImage;
@property (strong, nonatomic) NSImage *wormHoleDragImage;
@property (strong, nonatomic) NSImage *positiveMonoPoleOnImage;
@property (strong, nonatomic) NSImage *positiveMonoPoleOnDragImage;
@property (strong, nonatomic) NSImage *positiveMonoPoleOffImage;
@property (strong, nonatomic) NSImage *positiveMonoPoleOffDragImage;
@property (strong, nonatomic) NSImage *negativeMonoPoleOnImage;
@property (strong, nonatomic) NSImage *negativeMonoPoleOnDragImage;
@property (strong, nonatomic) NSImage *negativeMonoPoleOffImage;
@property (strong, nonatomic) NSImage *negativeMonoPoleOffDragImage;
@property (strong, nonatomic) NSImage *coneImage;
@property (strong, nonatomic) NSImage *coneDragImage;
@property (strong, nonatomic) NSImage *canImage;
@property (strong, nonatomic) NSImage *canDragImage;
@property (strong, nonatomic) NSImage *spaceImage;
@property (strong, nonatomic) NSImage *spaceDragImage;
@property (strong, nonatomic) NSImage *dirtImage;
@property (strong, nonatomic) NSImage *dirtDragImage;
@property (strong, nonatomic) NSImage *grassImage;
@property (strong, nonatomic) NSImage *grassDragImage;
@property (strong, nonatomic) NSImage *gravelImage;
@property (strong, nonatomic) NSImage *gravelDragImage;
@property (strong, nonatomic) NSImage *sandImage;
@property (strong, nonatomic) NSImage *sandDragImage;
@property (strong, nonatomic) NSImage *mineImage;
@property (strong, nonatomic) NSImage *mineDragImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterTAImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterTADragImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterTBImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterTBDragImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterTCImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterTCDragImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterTDImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterTDDragImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterEAImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterEADragImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterEBImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterEBDragImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterECImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterECDragImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterEDImage;
@property (strong, nonatomic) NSImage *wormHoleTransporterEDDragImage;
@property (strong, nonatomic) NSImage *trapTransporterTAImage;
@property (strong, nonatomic) NSImage *trapTransporterTADragImage;
@property (strong, nonatomic) NSImage *trapTransporterTBImage;
@property (strong, nonatomic) NSImage *trapTransporterTBDragImage;
@property (strong, nonatomic) NSImage *trapTransporterTCImage;
@property (strong, nonatomic) NSImage *trapTransporterTCDragImage;
@property (strong, nonatomic) NSImage *trapTransporterTDImage;
@property (strong, nonatomic) NSImage *trapTransporterTDDragImage;
@property (strong, nonatomic) NSImage *trapTransporterEAImage;
@property (strong, nonatomic) NSImage *trapTransporterEADragImage;
@property (strong, nonatomic) NSImage *trapTransporterEBImage;
@property (strong, nonatomic) NSImage *trapTransporterEBDragImage;
@property (strong, nonatomic) NSImage *trapTransporterECImage;
@property (strong, nonatomic) NSImage *trapTransporterECDragImage;
@property (strong, nonatomic) NSImage *trapTransporterEDImage;
@property (strong, nonatomic) NSImage *trapTransporterEDDragImage;
@property (strong, nonatomic) NSImage *pinRedImage;
@property (strong, nonatomic) NSImage *pinRedDragImage;
@property (strong, nonatomic) NSImage *pinGreenImage;
@property (strong, nonatomic) NSImage *pinGreenDragImage;
@property (strong, nonatomic) NSImage *pinYellowImage;
@property (strong, nonatomic) NSImage *pinYellowDragImage;
@property (strong, nonatomic) NSImage *pinBlueImage;
@property (strong, nonatomic) NSImage *pinBlueDragImage;
@property (strong, nonatomic) NSImage *explosiveImage;
@property (strong, nonatomic) NSImage *explosiveDragImage;
@property (strong, nonatomic) NSImage *detenatorImage;
@property (strong, nonatomic) NSImage *detenatorDragImage;
@property (strong, nonatomic) NSImage *bombImage;
@property (strong, nonatomic) NSImage *bombDragImage;
@property (strong, nonatomic) NSImage *megaBombImage;
@property (strong, nonatomic) NSImage *megaBombDragImage;
@property (strong, nonatomic) NSImage *redSpinnerImage;
@property (strong, nonatomic) NSImage *redSpinnerDragImage;
@property (strong, nonatomic) NSImage *greenSpinnerImage;
@property (strong, nonatomic) NSImage *greenSpinnerDragImage;
@property (strong, nonatomic) NSImage *umbrellaImage;
@property (strong, nonatomic) NSImage *umbrellaDragImage;
@property (strong, nonatomic) NSImage *repulsionImage;
@property (strong, nonatomic) NSImage *repulsionDragImage;

@end

@implementation HYImages

- (id)init
{
    self = [super init];
    if (self) {
		// Load the background image.
		self.backgroundImage = [NSImage imageNamed:@"Background"];
		// Load the images. All these objects are in the autorelease pool.
		self.electronImage = [NSImage imageNamed:@"Electron"];
		self.electronDragImage = [NSImage imageNamed:@"Electron-dragged"];
		self.electronChargedImage = [NSImage imageNamed:@"Electron-charged"];
		self.electronChargedDragImage = [NSImage imageNamed:@"Electron-charged-dragged"];
		self.protonImage = [NSImage imageNamed:@"Proton"];
		self.protonDragImage = [NSImage imageNamed:@"Proton-dragged"];
		self.protonChargedImage = [NSImage imageNamed:@"Proton-charged"];
		self.protonChargedDragImage = [NSImage imageNamed:@"Proton-charged-dragged"];
		self.abyssImage = [NSImage imageNamed:@"Abyss-fl"];
		self.abyssDragImage = [NSImage imageNamed:@"Abyss-fl-dragged"];
		self.chargeStationImage = [NSImage imageNamed:@"Charge0"];
		self.chargeStationDragImage = [NSImage imageNamed:@"Charge0-dragged"];
		self.craterImage = [NSImage imageNamed:@"Crater"];
		self.craterDragImage = [NSImage imageNamed:@"Crater-dragged"];
		self.homeProtonImage = [NSImage imageNamed:@"Home-proton"];
		self.homeProtonDragImage = [NSImage imageNamed:@"Home-proton-dragged"];
		self.homeElectronImage = [NSImage imageNamed:@"Home-electron"];
		self.homeElectronDragImage = [NSImage imageNamed:@"Home-electron-dragged"];
		self.homeImage = [NSImage imageNamed:@"Home-both"];
		self.homeDragImage = [NSImage imageNamed:@"Home-both-dragged"];
		self.greyFloorImage = [NSImage imageNamed:@"Grey-floor"];
		self.greyFloorDragImage = [NSImage imageNamed:@"Grey-floor-dragged"];
		self.woodFlTileHorImage = [NSImage imageNamed:@"Wood-fl-tile-hor"];
		self.woodFlTileHorDragImage = [NSImage imageNamed:@"Wood-fl-tile-hor-dragged"];
		self.woodFlTileVertImage = [NSImage imageNamed:@"Wood-fl-tile-vert"];
		self.woodFlTileVertDragImage = [NSImage imageNamed:@"Wood-fl-tile-vert-dragged"];
		self.woodPanelAImage = [NSImage imageNamed:@"Wood-panel-a"];
		self.woodPanelADragImage = [NSImage imageNamed:@"Wood-panel-a-dragged"];
		self.woodPanelBImage = [NSImage imageNamed:@"Wood-panel-b"];
		self.woodPanelBDragImage = [NSImage imageNamed:@"Wood-panel-b-dragged"];
		self.woodPanelCImage = [NSImage imageNamed:@"Wood-panel-c"];
		self.woodPanelCDragImage = [NSImage imageNamed:@"Wood-panel-c-dragged"];
		self.marbleFlAImage = [NSImage imageNamed:@"Marble-fl-a"];
		self.marbleFlADragImage = [NSImage imageNamed:@"Marble-fl-a-dragged"];
		self.marbleFlBImage = [NSImage imageNamed:@"Marble-fl-b"];
		self.marbleFlBDragImage = [NSImage imageNamed:@"Marble-fl-b-dragged"];
		self.marbleFlCImage = [NSImage imageNamed:@"Marble-fl-c"];
		self.marbleFlCDragImage = [NSImage imageNamed:@"Marble-fl-c-dragged"];
		self.marbleFlDImage = [NSImage imageNamed:@"Marble-fl-d"];
		self.marbleFLDDragImage = [NSImage imageNamed:@"Marble-fl-d-dragged"];
		self.groundStationImage = [NSImage imageNamed:@"Ground0"];
		self.groundStationDragImage = [NSImage imageNamed:@"Ground0-dragged"];
		self.messageTileImage = [NSImage imageNamed:@"Message"];
		self.messageTileDragImage = [NSImage imageNamed:@"Message-dragged"];
		self.turquoiseTileImage = [NSImage imageNamed:@"Charm-1"];
		self.turquoiseTileDragImage = [NSImage imageNamed:@"Charm-1-dragged"];
		self.rubyTileImage = [NSImage imageNamed:@"Charm-2"];
		self.rubyTileDragImage = [NSImage imageNamed:@"Charm-2-dragged"];
		self.silverTileImage = [NSImage imageNamed:@"Silver"];
		self.silverTileDragImage = [NSImage imageNamed:@"Silver-dragged"];
		self.goldTileImage = [NSImage imageNamed:@"Gold"];
		self.goldTileDragImage = [NSImage imageNamed:@"Gold-dragged"];
		self.invisibilityTileImage = [NSImage imageNamed:@"Invisibility-Potion"];
		self.invisibilityTileDragImage = [NSImage imageNamed:@"Invisibility-Potion-dragged"];
		self.shieldTileImage = [NSImage imageNamed:@"Shields"];
		self.shieldTileDragImage = [NSImage imageNamed:@"Shields-dragged"];
		self.stoneImage = [NSImage imageNamed:@"Stone"];
		self.stoneDragImage = [NSImage imageNamed:@"Stone-dragged"];
		self.stoneAImage = [NSImage imageNamed:@"Stone-a"];
		self.stoneADragImage = [NSImage imageNamed:@"Stone-a-dragged"];
		self.stoneBImage = [NSImage imageNamed:@"Stone-b"];
		self.stoneBDragImage = [NSImage imageNamed:@"Stone-b-dragged"];
		self.stoneCImage = [NSImage imageNamed:@"Stone-c"];
		self.stoneCDragImage = [NSImage imageNamed:@"Stone-c-dragged"];
		self.waterImage = [NSImage imageNamed:@"Water"];
		self.waterDragImage = [NSImage imageNamed:@"Water-dragged"];
		self.switchOnImage = [NSImage imageNamed:@"Switch-On"];
		self.switchOnDragImage = [NSImage imageNamed:@"Switch-On-dragged"];
		self.switchOffImage = [NSImage imageNamed:@"Switch-Off"];
		self.switchOffDragImage = [NSImage imageNamed:@"Switch-Off-dragged"];
		self.woodImage = [NSImage imageNamed:@"Wood-st-hor"];
		self.woodDragImage = [NSImage imageNamed:@"Wood-st-hor-dragged"];
		self.woodFlImage = [NSImage imageNamed:@"Wood-fl-hor"];
		self.woodFlDragImage = [NSImage imageNamed:@"Wood-fl-hor-dragged"];
		self.forceFieldOnImage = [NSImage imageNamed:@"Force-field-on"];
		self.forceFieldOnDragImage = [NSImage imageNamed:@"Force-field-on-dragged"];
		self.forceFieldOffImage = [NSImage imageNamed:@"Force-field-off"];
		self.forceFieldOffDragImage = [NSImage imageNamed:@"Force-field-off-dragged"];
		self.forceFieldImage = [NSImage imageNamed:@"Force-field0"];
		self.trapFlHorOpenImage = [NSImage imageNamed:@"Trap-fl-hor-3"];
		self.trapFlHorOpenDragImage = [NSImage imageNamed:@"Trap-fl-hor-3-dragged"];
		self.trapFlHorClosedImage = [NSImage imageNamed:@"Trap-fl-hor-0"];
		self.trapFlHorClosedDragImage = [NSImage imageNamed:@"Trap-fl-hor-0-dragged"];
		self.trapFlVertOpenImage = [NSImage imageNamed:@"Trap-fl-ver-3"];
		self.trapFlVertOpenDragImage = [NSImage imageNamed:@"Trap-fl-ver-3-dragged"];
		self.trapFlVertClosedImage = [NSImage imageNamed:@"Trap-fl-ver-0"];
		self.trapFlVertClosedDragImage = [NSImage imageNamed:@"Trap-fl-ver-0-dragged"];
		self.wormHoleImage = [NSImage imageNamed:@"Hyper-Space"];
		self.wormHoleDragImage = [NSImage imageNamed:@"Hyper-Space-dragged"];
		self.positiveMonoPoleOnImage = [NSImage imageNamed:@"Electric-Monopole-plus-on"];
		self.positiveMonoPoleOnDragImage = [NSImage imageNamed:@"Electric-Monopole-plus-on-dragged"];
		self.positiveMonoPoleOffImage = [NSImage imageNamed:@"Electric-Monopole-plus-off"];
		self.positiveMonoPoleOffDragImage = [NSImage imageNamed:@"Electric-Monopole-plus-off-dragged"];
		self.negativeMonoPoleOnImage = [NSImage imageNamed:@"Electric-Monopole-minus-on"];
		self.negativeMonoPoleOnDragImage = [NSImage imageNamed:@"Electric-Monopole-minus-on-dragged"];
		self.negativeMonoPoleOffImage = [NSImage imageNamed:@"Electric-Monopole-minus-off"];
		self.negativeMonoPoleOffDragImage = [NSImage imageNamed:@"Electric-Monopole-minus-off-dragged"];
		self.coneImage = [NSImage imageNamed:@"Cone"];
		self.coneDragImage = [NSImage imageNamed:@"Cone-dragged"];
		self.canImage = [NSImage imageNamed:@"Can"];
		self.canDragImage = [NSImage imageNamed:@"Can-dragged"];
		self.spaceImage = [NSImage imageNamed:@"Space"];
		self.spaceDragImage = [NSImage imageNamed:@"Space-dragged"];
		self.dirtImage = [NSImage imageNamed:@"Dirt"];
		self.dirtDragImage = [NSImage imageNamed:@"Dirt-dragged"];
		self.grassImage = [NSImage imageNamed:@"Grass"];
		self.grassDragImage = [NSImage imageNamed:@"Grass-dragged"];
		self.gravelImage = [NSImage imageNamed:@"Gravel"];
		self.gravelDragImage = [NSImage imageNamed:@"Gravel-dragged"];
		self.sandImage = [NSImage imageNamed:@"Sand"];
		self.sandDragImage = [NSImage imageNamed:@"Sand-dragged"];
		self.mineImage = [NSImage imageNamed:@"Mine"];
		self.mineDragImage = [NSImage imageNamed:@"Mine-dragged"];
		self.wormHoleTransporterTAImage = [NSImage imageNamed:@"Hyper-Space-transporter-TA"];
		self.wormHoleTransporterTADragImage = [NSImage imageNamed:@"Hyper-Space-transporter-TA-dragged"];
		self.wormHoleTransporterTBImage = [NSImage imageNamed:@"Hyper-Space-transporter-TB"];
		self.wormHoleTransporterTBDragImage = [NSImage imageNamed:@"Hyper-Space-transporter-TB-dragged"];
		self.wormHoleTransporterTCImage = [NSImage imageNamed:@"Hyper-Space-transporter-TC"];
		self.wormHoleTransporterTCDragImage = [NSImage imageNamed:@"Hyper-Space-transporter-TC-dragged"];
		self.wormHoleTransporterTDImage = [NSImage imageNamed:@"Hyper-Space-transporter-TD"];
		self.wormHoleTransporterTDDragImage = [NSImage imageNamed:@"Hyper-Space-transporter-TD-dragged"];
		self.wormHoleTransporterEAImage = [NSImage imageNamed:@"Hyper-Space-transporter-EA"];
		self.wormHoleTransporterEADragImage = [NSImage imageNamed:@"Hyper-Space-transporter-EA-dragged"];
		self.wormHoleTransporterEBImage = [NSImage imageNamed:@"Hyper-Space-transporter-EB"];
		self.wormHoleTransporterEBDragImage = [NSImage imageNamed:@"Hyper-Space-transporter-EB-dragged"];
		self.wormHoleTransporterECImage = [NSImage imageNamed:@"Hyper-Space-transporter-EC"];
		self.wormHoleTransporterECDragImage = [NSImage imageNamed:@"Hyper-Space-transporter-EC-dragged"];
		self.wormHoleTransporterEDImage = [NSImage imageNamed:@"Hyper-Space-transporter-ED"];
		self.wormHoleTransporterEDDragImage = [NSImage imageNamed:@"Hyper-Space-transporter-ED-dragged"];
		self.trapTransporterTAImage = [NSImage imageNamed:@"Trap-transporter-TA"];
		self.trapTransporterTADragImage = [NSImage imageNamed:@"Trap-transporter-TA-dragged"];
		self.trapTransporterTBImage = [NSImage imageNamed:@"Trap-transporter-TB"];
		self.trapTransporterTBDragImage = [NSImage imageNamed:@"Trap-transporter-TB-dragged"];
		self.trapTransporterTCImage = [NSImage imageNamed:@"Trap-transporter-TC"];
		self.trapTransporterTCDragImage = [NSImage imageNamed:@"Trap-transporter-TC-dragged"];
		self.trapTransporterTDImage = [NSImage imageNamed:@"Trap-transporter-TD"];
		self.trapTransporterTDDragImage = [NSImage imageNamed:@"Trap-transporter-TD-dragged"];
		self.trapTransporterEAImage = [NSImage imageNamed:@"Trap-transporter-EA"];
		self.trapTransporterEADragImage = [NSImage imageNamed:@"Trap-transporter-EA-dragged"];
		self.trapTransporterEBImage = [NSImage imageNamed:@"Trap-transporter-EB"];
		self.trapTransporterEBDragImage = [NSImage imageNamed:@"Trap-transporter-EB-dragged"];
		self.trapTransporterECImage = [NSImage imageNamed:@"Trap-transporter-EC"];
		self.trapTransporterECDragImage = [NSImage imageNamed:@"Trap-transporter-EC-dragged"];
		self.trapTransporterEDImage = [NSImage imageNamed:@"Trap-transporter-ED"];
		self.trapTransporterEDDragImage = [NSImage imageNamed:@"Trap-transporter-ED-dragged"];
		self.pinRedImage = [NSImage imageNamed:@"Red-Orb"];
		self.pinRedDragImage = [NSImage imageNamed:@"Red-Orb-dragged"];
		self.pinGreenImage = [NSImage imageNamed:@"Green-Orb"];
		self.pinGreenDragImage = [NSImage imageNamed:@"Green-Orb-dragged"];
		self.pinYellowImage = [NSImage imageNamed:@"Yellow-Orb"];
		self.pinYellowDragImage = [NSImage imageNamed:@"Yellow-Orb-dragged"];
		self.pinBlueImage = [NSImage imageNamed:@"Blue-Orb"];
		self.pinBlueDragImage = [NSImage imageNamed:@"Blue-Orb-dragged"];
		self.explosiveImage = [NSImage imageNamed:@"Explosive-fixed"];
		self.explosiveDragImage = [NSImage imageNamed:@"Explosive-fixed-dragged"];
		self.detenatorImage = [NSImage imageNamed:@"Detonator"];
		self.detenatorDragImage = [NSImage imageNamed:@"Detonator-dragged"];
		self.bombImage = [NSImage imageNamed:@"Bomb.png"];
		self.bombDragImage = [NSImage imageNamed:@"Bomb-dragged.png"];
		self.megaBombImage = [NSImage imageNamed:@"Mega-bomb"];
		self.megaBombDragImage = [NSImage imageNamed:@"Mega-bomb-dragged"];
		self.redSpinnerImage = [NSImage imageNamed:@"Spinner1"];
		self.redSpinnerDragImage = [NSImage imageNamed:@"Spinner1-dragged"];
		self.greenSpinnerImage = [NSImage imageNamed:@"Spinner2"];
		self.greenSpinnerDragImage = [NSImage imageNamed:@"Spinner2-dragged"];
        self.umbrellaImage = [NSImage imageNamed:@"umbrella"];
        self.umbrellaDragImage = [NSImage imageNamed:@"umbrella-dragged"];
        self.repulsionImage = [NSImage imageNamed:@"repulsion"];
        self.repulsionDragImage = [NSImage imageNamed:@"repulsion-dragged"];
    }
    return self;
}

- (void)assignImagesToTile:(HYPalette *)tile {
	tile.floorImage = nil;
	tile.dragImage = nil;
	tile.image = nil;
	tile.tentImage = nil;
	tile.snapShotImage = nil;
	int value = HYFloorOnlyMask & tile.value;
	switch (value) {
		case HYFloor:
			// No image
			break;
		case HYGrayFloor:
			tile.floorImage = self.greyFloorImage;
			tile.dragImage = self.greyFloorDragImage;
			tile.snapShotImage = self.greyFloorImage;
			break;
		case HYWoodFlHor:
			tile.floorImage = self.woodFlTileHorImage;
			tile.dragImage = self.woodFlTileHorDragImage;
			tile.snapShotImage = self.woodFlTileHorImage;
			break;
		case HYWoodFlVert:
			tile.floorImage = self.woodFlTileVertImage;
			tile.dragImage = self.woodFlTileVertDragImage;
			tile.snapShotImage = self.woodFlTileVertImage;
			break;
		case HYWoodPanelA:
			tile.floorImage = self.woodPanelAImage;
			tile.dragImage = self.woodPanelADragImage;
			tile.snapShotImage = self.woodPanelAImage;
			break;
		case HYWoodPanelB:
			tile.floorImage = self.woodPanelBImage;
			tile.dragImage = self.woodPanelBDragImage;
			tile.snapShotImage = self.woodPanelBImage;
			break;
		case HYWoodPanelC:
			tile.floorImage = self.woodPanelCImage;
			tile.dragImage = self.woodPanelCDragImage;
			tile.snapShotImage = self.woodPanelCImage;
			break;
		case HYMarbleFlA:
			tile.floorImage = self.marbleFlAImage;
			tile.dragImage = self.marbleFlADragImage;
			tile.snapShotImage = self.marbleFlAImage;
			break;
		case HYMarbleFlB:
			tile.floorImage = self.marbleFlBImage;
			tile.dragImage = self.marbleFlBDragImage;
			tile.snapShotImage = self.marbleFlBImage;
			break;
		case HYMarbleFlC:
			tile.floorImage = self.marbleFlCImage;
			tile.dragImage = self.marbleFlCDragImage;
			tile.snapShotImage = self.marbleFlCImage;
			break;
		case HYMarbleFlD:
			tile.floorImage = self.marbleFlDImage;
			tile.dragImage = self.marbleFLDDragImage;
			tile.snapShotImage = self.marbleFlDImage;
			break;
		case HYWater:
			tile.floorImage = self.waterImage;
			tile.dragImage = self.waterDragImage;
			tile.snapShotImage = self.waterImage;
			break;
		case HYAbyss:
			tile.floorImage = self.abyssImage;
			tile.dragImage = self.abyssDragImage;
			tile.snapShotImage = self.abyssImage;
			break;
		case HYTrapFlHorOpen:
			tile.floorImage = self.trapFlHorOpenImage;
			tile.snapShotImage = self.abyssImage;
			tile.dragImage = self.trapFlHorOpenDragImage;
			break;
		case HYTrapFlHorClosed:
			tile.floorImage = self.trapFlHorClosedImage;
			tile.snapShotImage = self.trapFlHorClosedImage;
			tile.dragImage = self.trapFlHorClosedDragImage;
			break;
		case HYTrapFlVertOpen:
			tile.floorImage = self.trapFlVertOpenImage;
			tile.snapShotImage = self.abyssImage;
			tile.dragImage = self.trapFlVertOpenDragImage;
			break;
		case HYTrapFlVertClosed:
			tile.floorImage = self.trapFlVertClosedImage;
			tile.snapShotImage = self.trapFlVertClosedImage;
			tile.dragImage = self.trapFlVertClosedDragImage;
			break;
		case HYTrapTransporterTA:
			tile.floorImage = self.trapTransporterTAImage;
			tile.dragImage = self.trapTransporterTADragImage;
			tile.snapShotImage = self.trapFlHorClosedImage;
			break;
		case HYTrapTransporterTB:
			tile.floorImage = self.trapTransporterTBImage;
			tile.dragImage = self.trapTransporterTBDragImage;
			tile.snapShotImage = self.trapFlHorClosedImage;
			break;		
		case HYTrapTransporterTC:
			tile.floorImage = self.trapTransporterTCImage;
			tile.dragImage = self.trapTransporterTCDragImage;
			tile.snapShotImage = self.trapFlHorClosedImage;
			break;		
		case HYTrapTransporterTD:
			tile.floorImage = self.trapTransporterTDImage;
			tile.dragImage = self.trapTransporterTDDragImage;
			tile.snapShotImage = self.trapFlHorClosedImage;
			break;		
		case HYTrapTransporterEA:
			tile.floorImage = self.trapTransporterEAImage;
			tile.dragImage = self.trapTransporterEADragImage;
			tile.snapShotImage = self.trapFlHorClosedImage;
			break;		
		case HYTrapTransporterEB:
			tile.floorImage = self.trapTransporterEBImage;
			tile.dragImage = self.trapTransporterEBDragImage;
			tile.snapShotImage = self.trapFlHorClosedImage;
			break;		
		case HYTrapTransporterEC:
			tile.floorImage = self.trapTransporterECImage;
			tile.dragImage = self.trapTransporterECDragImage;
			tile.snapShotImage = self.trapFlHorClosedImage;
			break;		
		case HYTrapTransporterED:
			tile.floorImage = self.trapTransporterEDImage;
			tile.dragImage = self.trapTransporterEDDragImage;
			tile.snapShotImage = self.trapFlHorClosedImage;
			break;		
		case HYSpace:
			tile.floorImage = self.spaceImage;
			tile.snapShotImage = self.spaceImage;
			tile.dragImage = self.spaceDragImage;
			break;
		case HYDirt:
			tile.floorImage = self.dirtImage;
			tile.snapShotImage = self.dirtImage;
			tile.dragImage = self.dirtDragImage;
			break;
		case HYGrass:
			tile.floorImage = self.grassImage;
			tile.snapShotImage = self.grassImage;
			tile.dragImage = self.grassDragImage;
			break;
		case HYGravel:
			tile.floorImage = self.gravelImage;
			tile.snapShotImage = self.gravelImage;
			tile.dragImage = self.gravelDragImage;
			break;
		case HYSand:
			tile.floorImage = self.sandImage;
			tile.snapShotImage = self.sandImage;
			tile.dragImage = self.sandDragImage;
			break;
		case HYWormholeTransporterTA:
			tile.floorImage = self.wormHoleTransporterTAImage;
			tile.dragImage = self.wormHoleTransporterTADragImage;
			tile.snapShotImage = self.wormHoleImage;
			break;
		case HYWormholeTransporterTB:
			tile.floorImage = self.wormHoleTransporterTBImage;
			tile.dragImage = self.wormHoleTransporterTBDragImage;
			tile.snapShotImage = self.wormHoleImage;
			break;		
		case HYWormholeTransporterTC:
			tile.floorImage = self.wormHoleTransporterTCImage;
			tile.dragImage = self.wormHoleTransporterTCDragImage;
			tile.snapShotImage = self.wormHoleImage;
			break;
		case HYWormholeTransporterTD:
			tile.floorImage = self.wormHoleTransporterTDImage;
			tile.dragImage = self.wormHoleTransporterTDDragImage;
			tile.snapShotImage = self.wormHoleImage;
			break;		
		case HYWormholeTransporterEA:
			tile.floorImage = self.wormHoleTransporterEAImage;
			tile.dragImage = self.wormHoleTransporterEADragImage;
			tile.snapShotImage = self.wormHoleImage;
			break;
		case HYWormholeTransporterEB:
			tile.floorImage = self.wormHoleTransporterEBImage;
			tile.dragImage = self.wormHoleTransporterEBDragImage;
			tile.snapShotImage = self.wormHoleImage;
			break;		
		case HYWormholeTransporterEC:
			tile.floorImage = self.wormHoleTransporterECImage;
			tile.dragImage = self.wormHoleTransporterECDragImage;
			tile.snapShotImage = self.wormHoleImage;
			break;
		case HYWormholeTransporterED:
			tile.floorImage = self.wormHoleTransporterEDImage;
			tile.dragImage = self.wormHoleTransporterEDDragImage;
			tile.snapShotImage = self.wormHoleImage;
			break;		
	}
	value = HYTileOnlyMask & tile.value;
	switch (value) {
		case HYStone:
			tile.image = self.stoneImage;
			tile.dragImage = self.stoneDragImage;
			tile.snapShotImage = self.stoneImage;
			break;
		case HYStoneA:
			tile.image = self.stoneAImage;
			tile.dragImage = self.stoneADragImage;
			tile.snapShotImage = self.stoneAImage;
			break;
		case HYStoneB:
			tile.image = self.stoneBImage;
			tile.dragImage = self.stoneBDragImage;
			tile.snapShotImage = self.stoneBImage;
			break;
		case HYStoneC:
			tile.image = self.stoneCImage;
			tile.dragImage = self.stoneCDragImage;
			tile.snapShotImage = self.stoneCImage;
			break;
		case HYCharger:
			tile.image = self.chargeStationImage;
			tile.dragImage = self.chargeStationDragImage;
			tile.snapShotImage = self.chargeStationImage;
			break;
		case HYCrater:
			tile.image = self.craterImage;
			tile.dragImage = self.craterDragImage;
			tile.snapShotImage = self.craterImage;
			break;
		case HYHome:
			tile.image = self.homeImage;
			tile.dragImage = self.homeDragImage;
			tile.snapShotImage = self.craterImage;
			break;
		case HYHomeProton:
			tile.image = self.homeProtonImage;
			tile.dragImage = self.homeProtonDragImage;
			tile.snapShotImage = self.craterImage;
			break;
		case HYHomeElectron:
			tile.image = self.homeElectronImage;
			tile.dragImage = self.homeElectronDragImage;
			tile.snapShotImage = self.craterImage;
			break;
		case HYDischarger:
			tile.image = self.groundStationImage;
			tile.dragImage = self.groundStationDragImage;
			tile.snapShotImage = self.groundStationImage;
			break;
		case HYMessageTile:
			tile.image = self.messageTileImage;
			tile.dragImage = self.messageTileDragImage;
			tile.snapShotImage = self.messageTileImage;
			break;
		case HYTurquoise:
			tile.image = self.turquoiseTileImage;
			tile.dragImage = self.turquoiseTileDragImage;
			tile.snapShotImage = self.turquoiseTileImage;
			break;
		case HYRuby:
			tile.image = self.rubyTileImage;
			tile.dragImage = self.rubyTileDragImage;
			tile.snapShotImage = self.rubyTileImage;
			break;
		case HYSilver:
			tile.image = self.silverTileImage;
			tile.dragImage = self.silverTileDragImage;
			tile.snapShotImage = self.silverTileImage;
			break;
		case HYGold:
			tile.image = self.goldTileImage;
			tile.dragImage = self.goldTileDragImage;
			tile.snapShotImage = self.goldTileImage;
			break;
		case HYInvisibility:
			tile.image = self.invisibilityTileImage;
			tile.dragImage = self.invisibilityTileDragImage;
			tile.snapShotImage = self.invisibilityTileImage;
			break;
        case HYUmbrella:
            tile.image = self.umbrellaImage;
            tile.dragImage = self.umbrellaDragImage;
            tile.snapShotImage = self.umbrellaImage;
            break;
        case HYRepulsion:
            tile.image = self.repulsionImage;
            tile.dragImage = self.repulsionDragImage;
            tile.snapShotImage = self.repulsionImage;
            break;
		case HYShield:
			tile.image = self.shieldTileImage;
			tile.dragImage = self.shieldTileDragImage;
			tile.snapShotImage = self.shieldTileImage;
			break;
		case HYSwitchOn:
			tile.image = self.switchOnImage;
			tile.dragImage = self.switchOnDragImage;
			tile.snapShotImage = self.switchOnImage;
			break;
		case HYSwitchOff:
			tile.image = self.switchOffImage;
			tile.dragImage = self.switchOffDragImage;
			tile.snapShotImage = self.switchOffImage;
			break;
		case HYWormhole:
			tile.image = self.wormHoleImage;
			tile.dragImage = self.wormHoleDragImage;
			tile.snapShotImage = self.wormHoleImage;
			break;
		case HYPositiveMonoPoleOn:
			tile.image = self.positiveMonoPoleOnImage;
			tile.dragImage = self.positiveMonoPoleOnDragImage;
			tile.snapShotImage = self.positiveMonoPoleOnImage;
			break;
		case HYPositiveMonoPoleOff:
			tile.image = self.positiveMonoPoleOffImage;
			tile.dragImage = self.positiveMonoPoleOffDragImage;
			tile.snapShotImage = self.positiveMonoPoleOffImage;
			break;
		case HYNegativeMonoPoleOn:
			tile.image = self.negativeMonoPoleOnImage;
			tile.dragImage = self.negativeMonoPoleOnDragImage;
			tile.snapShotImage = self.negativeMonoPoleOnImage;
			break;
		case HYNegativeMonoPoleOff:
			tile.image = self.negativeMonoPoleOffImage;
			tile.dragImage = self.negativeMonoPoleOffDragImage;
			tile.snapShotImage = self.negativeMonoPoleOffImage;
			break;
		case HYMine:
			tile.image = self.mineImage;
			tile.dragImage = self.mineDragImage;
			tile.snapShotImage = self.mineImage;
			break;
		case HYRedPin:
			tile.image = self.pinRedImage;
			tile.dragImage = self.pinRedDragImage;
			tile.snapShotImage = self.pinRedImage;
			break;
		case HYGreenPin:
			tile.image = self.pinGreenImage;
			tile.dragImage = self.pinGreenDragImage;
			tile.snapShotImage = self.pinGreenImage;
			break;
		case HYYellowPin:
			tile.image = self.pinYellowImage;
			tile.dragImage = self.pinYellowDragImage;
			tile.snapShotImage = self.pinYellowImage;
			break;
		case HYBluePin:
			tile.image = self.pinBlueImage;
			tile.dragImage = self.pinBlueDragImage;
			tile.snapShotImage = self.pinBlueImage;
			break;
		case HYBomb:
			tile.image = self.bombImage;
			tile.dragImage = self.bombDragImage;
			tile.snapShotImage = self.bombImage;
			break;
		case HYMegaBomb:
			tile.image = self.megaBombImage;
			tile.dragImage = self.megaBombDragImage;
			tile.snapShotImage = self.megaBombImage;
			break;
		default:
			break;
	}	
	value = HYMoveableOnly & tile.value;
	switch (value) {
		case HYWood:
			tile.image = self.woodImage;
			tile.dragImage = self.woodDragImage;
			tile.snapShotImage = self.woodImage;
			break;
		case HYWoodFloor:
			tile.floorImage = self.woodFlImage;
			tile.dragImage = self.woodFlDragImage;
			tile.snapShotImage = self.woodFlImage;
			break;						
		case HYDetonator:
			tile.image = self.detenatorImage;
			tile.dragImage = self.detenatorDragImage;
			tile.snapShotImage = self.detenatorImage;
			break;			
		case HYExplosiveCharge:
			tile.image = self.explosiveImage;
			tile.dragImage = self.explosiveDragImage;
			tile.snapShotImage = self.explosiveImage;
			break;			
	}
	value = HYParticleMask & tile.value;
	switch (value) {
		case HYElectron:
			tile.image = self.electronImage;
			tile.dragImage = self.electronDragImage;
			tile.snapShotImage = self.electronImage;
			break;
		case HYElectronCharged:
			tile.image = self.electronChargedImage;
			tile.dragImage = self.electronChargedDragImage;
			tile.snapShotImage = self.electronImage;
			break;
		case HYProton:
			tile.image = self.protonImage;
			tile.dragImage = self.protonDragImage;
			tile.snapShotImage = self.protonImage;
			break;			
		case HYProtonCharged:
			tile.image = self.protonChargedImage;
			tile.dragImage = self.protonChargedDragImage;
			tile.snapShotImage = self.protonImage;
			break;
		case HYCone:
			tile.image = self.coneImage;
			tile.dragImage = self.coneDragImage;
			tile.snapShotImage = self.coneImage;
			break;
		case HYCan:
			tile.image = self.canImage;
			tile.dragImage = self.canDragImage;
			tile.snapShotImage = self.canImage;
			break;
		case HYSpinner1:
			tile.image = self.redSpinnerImage;
			tile.dragImage = self.redSpinnerDragImage;
			tile.snapShotImage = self.redSpinnerImage;
			break;
		case HYSpinner2:
			tile.image = self.greenSpinnerImage;
			tile.dragImage = self.greenSpinnerDragImage;
			tile.snapShotImage = self.greenSpinnerImage;
			break;
	}
	value = HYTentMask & tile.value;
	switch (value) {
		case HYForceFieldOn:
			tile.tentImage = self.forceFieldOnImage;
			tile.dragImage = self.forceFieldOnImage;
			tile.snapShotImage = self.forceFieldImage;
			break;			
		case HYForceFieldOff:
			tile.tentImage = self.forceFieldOffImage;
			tile.dragImage = self.forceFieldOffImage;
			tile.snapShotImage = nil;
			break;
	}
}

- (NSImage *)getBackgroundImage {
	return self.backgroundImage;
}

@end
