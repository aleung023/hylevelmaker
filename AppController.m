//
//  AppController.m
//  HYLevelMaker
//
//  Created by Albert  Leung on 4/3/09.
//  Copyright 2009 WhiffleBird. All rights reserved.
//

#import "AppController.h"
#import "TilePanelController.h"

@implementation AppController

- (id)init {
	self = [super init];
    if (self) {
        self.tilePanelController = [[TilePanelController alloc] init];
        [self.tilePanelController showWindow:self];
    }
	return self;
}

- (IBAction)showTilePanel:(id)sender {
	if (!self.tilePanelController) {
		self.tilePanelController = [[TilePanelController alloc] init];
	}
	[self.tilePanelController showWindow:self];
}

- (void)awakeFromNib {
}

@end
