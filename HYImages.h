//
//  HYImages.h
//  HYLevelMaker
//
//  Created by Albert on 4/12/09.
//  Copyright 2009 Whifflebird. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class HYPalette;

@interface HYImages : NSObject {
	
}

- (void)assignImagesToTile:(HYPalette *)tile;
- (NSImage *)getBackgroundImage;

@end
