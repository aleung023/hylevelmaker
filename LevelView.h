//
//  LevelView.h
//  HydrogenLevelMaker
//
//  Created by Albert  Leung on 3/31/09.
//  Copyright 2009 WhiffleBird. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class LevelTile;
@class HYImages;

#define kTileWidth				32.0
#define kBump					32.0
#define kMaxColumns				9
#define kMaxRows				13
#define kNumberOfLevelTiles		kMaxRows * kMaxColumns

@interface LevelView : NSView

@property (strong, nonatomic) NSMutableArray *tiles;
@property (strong, nonatomic) NSMutableArray *connections;
@property (assign, nonatomic) BOOL doSnapShot;

- (void)updateImagesToTiles;
- (void)recreateConnections:(NSMutableArray *)pairValues;

@end
